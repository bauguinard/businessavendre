import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListeFormationPage } from './liste-formation.page';

describe('ListeFormationPage', () => {
  let component: ListeFormationPage;
  let fixture: ComponentFixture<ListeFormationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeFormationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListeFormationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
