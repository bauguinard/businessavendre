import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListeFormationPageRoutingModule } from './liste-formation-routing.module';

import { ListeFormationPage } from './liste-formation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeFormationPageRoutingModule
  ],
  declarations: [ListeFormationPage]
})
export class ListeFormationPageModule {}
