import { Component, OnInit } from '@angular/core';
import { UtilsService } from "../services/utils.service";
import { FormationService } from '../services/formation.service';

@Component({
  selector: 'app-liste-formation',
  templateUrl: './liste-formation.page.html',
  styleUrls: ['./liste-formation.page.scss'],
})
export class ListeFormationPage implements OnInit {
  formations:any;
  formationFormat:any = [];

  constructor(private utilService: UtilsService,
    private formationService: FormationService) {

    }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getFormation();
  }

  ionViewWillLeave() {
    this.formationFormat = [];
  }

  getFormation(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.formationService.getFormations().then((response)=>{
        console.log(response.data);
        this.formations = response.data;
        this.formations.forEach(formation => {
          let dateMonth = formation.created_at;
          let dateArray = dateMonth.split(" ");
          let year = dateArray[0].split("-")[2];
          let month = dateArray[0].split("-")[1];
          let myDate = year+"/"+month;

          let objetAnnonce = {
            id:formation.id,
            created_at:myDate,
            title:formation.title
          };
          
          this.formationFormat.push(objetAnnonce);
        });
        console.log(this.formationFormat);
        loader.dismiss();
      })
    });
  }

}
