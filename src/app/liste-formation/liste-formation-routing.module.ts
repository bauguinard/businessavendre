import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeFormationPage } from './liste-formation.page';

const routes: Routes = [
  {
    path: '',
    component: ListeFormationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeFormationPageRoutingModule {}
