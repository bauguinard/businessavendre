import { Component, OnInit } from '@angular/core';
import { UtilsService } from "../services/utils.service";
import { GetInformationService } from "../services/get-information.service";
import { Storage } from '@ionic/storage';
import { Router } from "@angular/router";
import { AnnonceInvestisseurService } from '../services/annonce-investisseur.service';

@Component({
  selector: 'app-creer-annonce-investisseur',
  templateUrl: './creer-annonce-investisseur.page.html',
  styleUrls: ['./creer-annonce-investisseur.page.scss'],
})
export class CreerAnnonceInvestisseurPage implements OnInit {
  data:any = {
    fourchette_chriffes_affaires: '',
    fourchette_montant_a_investir: '',
    complementaire_transaction: '',
    pays: '',
    ville: '',
    user: '',
    secteuractivites:'',
    statut: 1,
    categorie:1,
  };

  montantMinCA:any;
  montantMaxCA:any;
  montantMinInvestir:any;
  montantMaxInvestir:any;
  categories:any;
  statuts:any;
  villes:any;
  pays:any;
  secteurs:any;

  constructor(private getInformationService: GetInformationService,
    private annonceInvestisseurService: AnnonceInvestisseurService,
    private utilService: UtilsService,
    private storage: Storage,
    private router: Router) {
      this.utilService.makeLoader("Patientez un instant svp!").then((loader)=>{
        loader.present();
        this.storage.get('user').then((user) => {
          this.data.user = user.id;
        });
        this.getInformationService.categories().then((categories)=>{
          console.log(categories.data);
          this.categories = categories.data;
        });
        loader.dismiss();
      })
   }

  ngOnInit() {
    this.getData();    
  }

  getData() {
    this.utilService.makeLoader("Patientez un instant svp!").then((loader)=>{
      loader.present();
      this.getInformationService.statut().then((statut)=>{
        console.log(statut);
        this.statuts = statut.data;
        this.getInformationService.pays().then((pays)=>{
          console.log(pays);
          this.pays = pays.data;
          this.getInformationService.ville().then((villes)=>{
            console.log(villes);
            this.villes = villes.data;
            loader.dismiss();
          }).catch((err)=>{
            console.log(err);
            loader.dismiss();
            this.utilService.makeAlert("Business à vendre","Votre connexion semble avoir un problème").then((alert)=>{
              alert.present();
            })
          })
        }).catch((err)=>{
          console.log(err);
          loader.dismiss();
          this.utilService.makeAlert("Business à vendre","Votre connexion semble avoir un problème").then((alert)=>{
            alert.present();
          })
        })
      })
    });
  }

  send(){
    if (this.montantMinCA != "" || this.montantMaxCA != "" || this.montantMinInvestir != "" || this.montantMaxInvestir != "" || this.data.complementaire_transaction != ""|| this.data.secteuractivites != "")
    {
      this.data.fourchette_chriffes_affaires = this.montantMinCA +"F CFA - "+this.montantMaxCA+"F CFA";
      this.data.fourchette_montant_a_investir = this.montantMinInvestir +"F CFA - "+this.montantMaxInvestir+"F CFA";
      console.log(this.data);
      this.utilService.makeLoader("Patientez un instant svp!").then((loader)=>{
        loader.present();
        this.annonceInvestisseurService.sendAnnonce(this.data).then((response)=>{
          loader.dismiss();
          console.log(response);
          this.utilService.makeToast("Votre annonce a bien été publiée!").then((toast)=>{
            toast.present().then(()=>{
              this.data.fourchette_chriffes_affaires = "";
              this.data.fourchette_montant_a_investir = "";
              this.data.complementaire_transaction = "";
              this.data.secteuractivites = "";
              this.montantMaxCA = "";
              this.montantMinCA = "";
              this.montantMaxInvestir = "";
              this.montantMinInvestir = "";
            });
            this.router.navigateByUrl('/list-annonce-investisseur');
          }).catch((error)=>{
            loader.dismiss();
            console.log(error);
            this.utilService.makeAlert("Business à vendre","Votre annonce n'a pu être publiée").then((alert)=>{
              alert.present();
            })
          })
        })
        .catch((err)=>{
          console.log(err);
          loader.dismiss();
          this.utilService.makeToast("Veuillez bien renseigner les champs svp!").then((toast)=>{
            toast.present();
          })
        })
      })
    }
    else
    {
      this.utilService.makeToast("Veuillez bien renseigner les champs svp!").then((toast)=>{
        toast.present();
      })
    }

  }


}
