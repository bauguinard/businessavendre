import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreerAnnonceInvestisseurPage } from './creer-annonce-investisseur.page';

const routes: Routes = [
  {
    path: '',
    component: CreerAnnonceInvestisseurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreerAnnonceInvestisseurPageRoutingModule {}
