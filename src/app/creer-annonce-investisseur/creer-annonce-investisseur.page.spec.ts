import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreerAnnonceInvestisseurPage } from './creer-annonce-investisseur.page';

describe('CreerAnnonceInvestisseurPage', () => {
  let component: CreerAnnonceInvestisseurPage;
  let fixture: ComponentFixture<CreerAnnonceInvestisseurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreerAnnonceInvestisseurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreerAnnonceInvestisseurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
