import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreerAnnonceInvestisseurPageRoutingModule } from './creer-annonce-investisseur-routing.module';

import { CreerAnnonceInvestisseurPage } from './creer-annonce-investisseur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreerAnnonceInvestisseurPageRoutingModule
  ],
  declarations: [CreerAnnonceInvestisseurPage]
})
export class CreerAnnonceInvestisseurPageModule {}
