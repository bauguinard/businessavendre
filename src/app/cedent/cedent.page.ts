import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../services/utils.service';
import { AnnonceCedantService } from "../services/annonce-cedant.service";

@Component({
  selector: 'app-cedent',
  templateUrl: './cedent.page.html',
  styleUrls: ['./cedent.page.scss'],
})
export class CedentPage implements OnInit {
  user:any;
  myAnnonces:any;
  annonces:any = [];

  constructor(private utilService: UtilsService,
    private annonceCedantService: AnnonceCedantService) {
    }

  ngOnInit() {
  }
  
  ionViewWillEnter() {
    this.getAnnonce();
  }

  ionViewWillLeave() {
    this.annonces = [];
  }

  doRefresh(event){
    this.annonces = [];
    this.getAnnonce();
    event.target.complete();
  }

  getAnnonce(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.annonceCedantService.getAllAnnonce().then((response)=>{
        console.log(response.data);
        this.myAnnonces = response.data;
        this.myAnnonces.forEach(annonce => {
          let dateMonth = annonce.created_at;
          let dateArray = dateMonth.split(" ");
          let year = dateArray[0].split("-")[2];
          let month = dateArray[0].split("-")[1];
          let myDate = year+"/"+month;

          let objetAnnonce = {
            id:annonce.id,
            created_at:myDate,
            chiffre_affaire:annonce.chiffre_affaire,
            activite:annonce.secteuractivites,
            ville:annonce.ville
          };
          
          this.annonces.push(objetAnnonce);
        });
        console.log(this.annonces);
        loader.dismiss();
      })
      .catch((err)=>{
        loader.dismiss();
      })
    })
  }

  numStr(a, b) {
    a = '' + a;
    b = b || ' ';
    var c = '',
        d = 0;
    while (a.match(/^0[0-9]/)) {
      a = a.substr(1);
    }
    for (var i = a.length-1; i >= 0; i--) {
      c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
      d++;
    }
    return c;
  }

}
