import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CedentPage } from './cedent.page';

describe('CedentPage', () => {
  let component: CedentPage;
  let fixture: ComponentFixture<CedentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CedentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CedentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
