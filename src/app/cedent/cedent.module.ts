import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CedentPageRoutingModule } from './cedent-routing.module';

import { CedentPage } from './cedent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CedentPageRoutingModule
  ],
  declarations: [CedentPage]
})
export class CedentPageModule {}
