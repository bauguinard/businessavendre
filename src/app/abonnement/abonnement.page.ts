import { Component, OnInit } from '@angular/core';
import { AbonnementService } from "../services/abonnement.service";
import { UtilsService } from '../services/utils.service';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-abonnement',
  templateUrl: './abonnement.page.html',
  styleUrls: ['./abonnement.page.scss'],
})
export class AbonnementPage implements OnInit {
  public offres = [];
  public response:Boolean = false;
  typeUser;
  constructor(private abonnementService: AbonnementService,
    private storage: Storage,
    private iab: InAppBrowser,
    private utilService: UtilsService,) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
      this.storage.get('user').then((user) => {
        if(typeof user == "object")
        {
          this.typeUser = user.type_id;
          this.getOffre();
        }
      });
  }

  getOffre(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.abonnementService.getAbonnement().then((response)=>{
        console.log(response.data);
        this.offres = response.data;
        loader.dismiss();
      })
      .catch((err)=>{
        loader.dismiss();
      })
    })
  }

  souscrire(id,cout){
    this.storage.get('user').then((user) => {
      console.log(user);
      if(typeof user == "object")
      {
        let credential:any = {
          user_id: user.id,
          offre_id: id,
          montant: cout
        };

        this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
          loader.present();
          this.abonnementService.getUrlPayment(credential).then((response)=>{
            console.log(response.data);
            loader.dismiss();
            this.iab.create(response.data.url);
          })
          .catch((err)=>{
            loader.dismiss();
          })
        })
        //this.router.navigate(['/paiement', id]);    
      }
      else
      {
        this.utilService.makeToast("Veuillez vous connecter avant pouvoir souscrire à une offre").then((toast)=>{
          toast.present();
        })
      }
    });
  }
}
