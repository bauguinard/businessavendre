import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaiementExpertPage } from './paiement-expert.page';

describe('PaiementExpertPage', () => {
  let component: PaiementExpertPage;
  let fixture: ComponentFixture<PaiementExpertPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaiementExpertPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaiementExpertPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
