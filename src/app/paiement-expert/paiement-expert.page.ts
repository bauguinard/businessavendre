import { Component, OnInit } from '@angular/core';
import { PaiementService } from '../services/paiement.service';
import { UtilsService } from '../services/utils.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AppComponent } from "../app.component";
import { AuthService } from "../services/auth.service";

@Component({
  selector: 'app-paiement-expert',
  templateUrl: './paiement-expert.page.html',
  styleUrls: ['./paiement-expert.page.scss'],
})
export class PaiementExpertPage implements OnInit {
  data:any = {
    username:'',
    email:'',
    telephone:'',
    adresse:'',
    civilite:'',
    date_naissance:'',
    password:'',
    password_confirmation:'',
    type_id:'',
    pays_id:'',
    ville_id:'',
    montant:'',
    operateur:'',
    offre_id:''
  };

  constructor(private router: Router,
    private utilService: UtilsService,
    private authService: AuthService,
    private appComponent: AppComponent,
    private storage: Storage,
    ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('data').then((data)=>{
      this.data.username = data.username;
      this.data.email = data.email;
      this.data.telephone = data.telephone;
      this.data.adresse = data.adresse;
      this.data.civilite = data.civilite;
      this.data.date_naissance = data.date_naissance;
      this.data.password = data.password;
      this.data.password_confirmation = data.password_confirmation;
      this.data.type_id = data.type_id;
      this.data.pays_id = data.pays_id;
      this.data.ville_id = data.ville_id;
      this.data.montant = 2200;
      this.data.offre_id = 1;
      console.log(this.data);
    })
  }

  checkout(){
    this.utilService.makeLoader("Patientez un instant").then((loader)=>{
      loader.present();
      if (/*this.data.contact != "" && */this.data.operateur != "") {
        this.authService.register(this.data).then((response)=>{
          this.storage.set('user',response.data).then(()=>{
            this.appComponent.connecte = true;
            loader.dismiss();
            this.router.navigate(['/home']);
          })
        })
        .catch((error)=>{
          loader.dismiss();
          this.utilService.makeAlert("BAV","Votre solde est insuffisant!").then((alert)=>{
            console.log(error);
            alert.present();
          })
        })
      }
      else{
        loader.dismiss();
        this.utilService.makeToast("Veuillez renseigner le contact et l'opérateur svp!").then((toast)=>{
          toast.present();
        })
      }
    })

  }

}
