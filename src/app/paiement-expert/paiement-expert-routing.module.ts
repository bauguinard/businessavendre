import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaiementExpertPage } from './paiement-expert.page';

const routes: Routes = [
  {
    path: '',
    component: PaiementExpertPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaiementExpertPageRoutingModule {}
