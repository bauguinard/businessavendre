import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaiementExpertPageRoutingModule } from './paiement-expert-routing.module';

import { PaiementExpertPage } from './paiement-expert.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaiementExpertPageRoutingModule
  ],
  declarations: [PaiementExpertPage]
})
export class PaiementExpertPageModule {}
