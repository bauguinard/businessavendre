import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TchatListDemandeurPage } from './tchat-list-demandeur.page';

describe('TchatListDemandeurPage', () => {
  let component: TchatListDemandeurPage;
  let fixture: ComponentFixture<TchatListDemandeurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TchatListDemandeurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TchatListDemandeurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
