import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TchatListDemandeurPage } from './tchat-list-demandeur.page';

const routes: Routes = [
  {
    path: '',
    component: TchatListDemandeurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TchatListDemandeurPageRoutingModule {}
