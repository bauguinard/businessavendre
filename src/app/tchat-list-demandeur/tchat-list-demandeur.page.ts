import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../services/utils.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { RelationService } from "../services/relation.service";

@Component({
  selector: 'app-tchat-list-demandeur',
  templateUrl: './tchat-list-demandeur.page.html',
  styleUrls: ['./tchat-list-demandeur.page.scss'],
})
export class TchatListDemandeurPage implements OnInit {
  relations:any = [];
  user:any;
  relation_id:any = ""
  type: string;

  constructor(private storage: Storage,
    private router: Router,
    private utilService: UtilsService,
    private relationService: RelationService) { }

    ngOnInit() {
      this.type = 'mes_demandes';
      this.relationService.getDemandeRelationDemandeur(this.user).then((relation)=>{
        this.relations = relation.data;

        console.log(relation.data);
      })
      .catch((error)=>{
        console.log(error);
      })
    }
  
    ionViewWillEnter() {
      this.getRelationDemandeur();
    }

    getRelationDemandeur(){
      this.storage.get('user').then((data) => {
        if(data.id != "")
        {
            this.user = data.id
            this.relationService.getDemandeRelationDemandeur(this.user).then((relation)=>{
              this.relations = relation.data;
  
              console.log(relation.data);
            })
            .catch((error)=>{
              console.log(error);
            })
        }
      });
    }

    tchat(relation){
      console.log(relation)
      if (relation.statut_id == 1) {
        this.storage.set('relation',relation).then((success)=>{
          this.router.navigate(['/tchat',relation.id]);
        });
      }
      else if(relation.statut_id == 2){
        this.utilService.makeToast("Votre demande n'a pas encore été validée, veuillez patientez que l'utilisateur valide votre demande").then((toast)=>{
          toast.present();
        });
      }
      else{
        this.utilService.makeToast("Votre demande a été réfusée").then((toast)=>{
          toast.present();
        });
      }
    }

    segmentChanged(ev: any) {
      if(ev.detail.value == "vos_demandes"){
        this.relationService.getDemandeRelationReception(this.user).then((relation)=>{
          this.relations = relation.data;

          console.log(relation.data);
        })
        .catch((error)=>{
          console.log(error);
        })
      }
      else{
        this.relationService.getDemandeRelationDemandeur(this.user).then((relation)=>{
          this.relations = relation.data;

          console.log(relation.data);
        })
        .catch((error)=>{
          console.log(error);
        })
      }
      console.log('Segment changed', ev);
    }


    validerDemande(id){
      this.relationService.validerDemande(id)
      .then((response)=>{
        console.log(response.data);
        this.relation_id = response.data.id;
        this.utilService.makeToast(response.message).then((toast)=>{
          toast.present();
          this.getRelationDemandeur();
        }) 
      })
      .catch((error)=>{
        console.log(error);
      })
    }

    annulerDemande(id){
      this.relationService.annulerDemande(id)
      .then((response)=>{
        console.log(response.data);
        this.relation_id = response.data.id;
        this.utilService.makeToast(response.message).then((toast)=>{
          toast.present();
          this.getRelationDemandeur();
        }) 
      })
      .catch((error)=>{
        console.log(error);
      })
    }
}
