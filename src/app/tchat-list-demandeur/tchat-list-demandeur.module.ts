import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TchatListDemandeurPageRoutingModule } from './tchat-list-demandeur-routing.module';

import { TchatListDemandeurPage } from './tchat-list-demandeur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TchatListDemandeurPageRoutingModule
  ],
  declarations: [TchatListDemandeurPage]
})
export class TchatListDemandeurPageModule {}
