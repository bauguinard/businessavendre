import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InvestisseurSearchPage } from './investisseur-search.page';

describe('InvestisseurSearchPage', () => {
  let component: InvestisseurSearchPage;
  let fixture: ComponentFixture<InvestisseurSearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestisseurSearchPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InvestisseurSearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
