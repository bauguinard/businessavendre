import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvestisseurSearchPageRoutingModule } from './investisseur-search-routing.module';

import { InvestisseurSearchPage } from './investisseur-search.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvestisseurSearchPageRoutingModule
  ],
  declarations: [InvestisseurSearchPage]
})
export class InvestisseurSearchPageModule {}
