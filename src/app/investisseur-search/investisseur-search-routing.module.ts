import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvestisseurSearchPage } from './investisseur-search.page';

const routes: Routes = [
  {
    path: '',
    component: InvestisseurSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvestisseurSearchPageRoutingModule {}
