import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../services/utils.service';
import { AnnonceInvestisseurService } from "../services/annonce-investisseur.service";
import { GetInformationService } from '../services/get-information.service';
import { AnnonceCedantService } from '../services/annonce-cedant.service';
@Component({
  selector: 'app-investisseur-search',
  templateUrl: './investisseur-search.page.html',
  styleUrls: ['./investisseur-search.page.scss'],
})
export class InvestisseurSearchPage implements OnInit {
  data:any = {
    fourchette_montant_a_investir: '',
    ville_id: "",
    secteuractivite:""
  };
  secteurs:any;
  villes:any;
  montantMinInvestir:any;
  montantMaxInvestir:any;
  myAnnonces:any;
  annonces:any = [];
  constructor(private utilService: UtilsService,
    private annonceInvestisseurService:AnnonceInvestisseurService,
    private getInformationService: GetInformationService,
    private annonceCedantService:AnnonceCedantService,) { }

  ngOnInit() {
    this.getVille();
  }

  getSecteurActivite() {
    this.annonceCedantService.getSecteurActivite().then((secteur)=>{
      console.log(secteur);
      this.secteurs = secteur.data;
    }).catch((error)=>{
      console.error(error);
    })
  }

  getVille() {
    this.utilService.makeLoader('Un instant svp!').then((loader)=>{
      loader.present();
      this.getInformationService.ville().then((villes)=>{
        console.log(villes);
        this.villes = villes.data;
        loader.dismiss();
      }).catch((error)=>{
        loader.dismiss();
      })
    });
  }

  search(){
    this.data.fourchette_montant_a_investir = this.montantMinInvestir +"F CFA - "+this.montantMaxInvestir+"F CFA";;

    this.utilService.makeLoader("Un instant svp...").then((loader)=>{
      loader.present();
      this.annonceInvestisseurService.search(this.data).then((response)=>{
        console.log(response);
        if (response.data.length > 0) {
          console.log(response.data);
          this.myAnnonces = response.data;
          this.myAnnonces.forEach(annonce => {
            let dateMonth = annonce.created_at;
            let dateArray = dateMonth.split(" ");
            let year = dateArray[0].split("-")[2];
            let month = dateArray[0].split("-")[1];
            let myDate = year+"/"+month;
  
            let objetAnnonce = {
              id:annonce.id,
              created_at:myDate,
              raison_transaction:annonce.raison_transaction
            };
            
            this.annonces.push(objetAnnonce);
          });
          loader.dismiss();
        }
        else{
          loader.dismiss();
          this.utilService.makeToast("Aucun investisseur ne répond à vos critères de recherche").then((toast)=>{
            toast.present();
          })
        }
      }).catch((error)=>{
        console.log(error);
        loader.dismiss();
      });
    });
  }

  numStr(a, b) {
    a = '' + a;
    b = b || ' ';
    var c = '',
        d = 0;
    while (a.match(/^0[0-9]/)) {
      a = a.substr(1);
    }
    for (var i = a.length-1; i >= 0; i--) {
      c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
      d++;
    }
    return c;
  }

  ionViewWillLeave() {
    this.annonces = [];
  }
}
