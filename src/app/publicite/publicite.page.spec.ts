import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicitePage } from './publicite.page';

describe('PublicitePage', () => {
  let component: PublicitePage;
  let fixture: ComponentFixture<PublicitePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicitePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicitePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
