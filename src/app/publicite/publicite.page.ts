import { Component, OnInit } from '@angular/core';
import { PubliciteService } from "../services/publicite.service";
import { UtilsService } from '../services/utils.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-publicite',
  templateUrl: './publicite.page.html',
  styleUrls: ['./publicite.page.scss'],
})
export class PublicitePage implements OnInit {

  public publicites:any = [];

  constructor(private publiciteService: PubliciteService, private utilService: UtilsService,private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getPublicite();
  }

  getPublicite(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.publiciteService.getPublicite().then((response)=>{
        console.log(response.data);
        this.publicites = response.data;
        loader.dismiss();
      })
      .catch((err)=>{
        loader.dismiss();
      })
    })
  }

  detail(id){
    this.router.navigate(['/publicite-detail', id]);
  }

}
