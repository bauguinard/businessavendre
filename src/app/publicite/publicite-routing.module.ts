import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicitePage } from './publicite.page';

const routes: Routes = [
  {
    path: '',
    component: PublicitePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicitePageRoutingModule {}
