import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicitePageRoutingModule } from './publicite-routing.module';

import { PublicitePage } from './publicite.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicitePageRoutingModule
  ],
  declarations: [PublicitePage]
})
export class PublicitePageModule {}
