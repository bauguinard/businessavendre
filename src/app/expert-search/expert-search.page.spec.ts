import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExpertSearchPage } from './expert-search.page';

describe('ExpertSearchPage', () => {
  let component: ExpertSearchPage;
  let fixture: ComponentFixture<ExpertSearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertSearchPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExpertSearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
