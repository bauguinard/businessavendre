import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../services/utils.service';
import { ExpertService } from "../services/expert.service";
import { GetInformationService } from "../services/get-information.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-expert-search',
  templateUrl: './expert-search.page.html',
  styleUrls: ['./expert-search.page.scss'],
})
export class ExpertSearchPage implements OnInit {
  pays:any = [];
  villes:any = [];
  data:any = {
    ville_id:"",
    domaine_competence:""
  }

  constructor(private utilService: UtilsService,
              private expertService: ExpertService,
              private getInformationService: GetInformationService,
              private router : Router) { }

  ngOnInit() {
    this.getVille();
  }

  ionViewWillEnter() {
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.getPays();
      this.getVille();
      loader.dismiss();
    })
  }

  getPays() {
    this.getInformationService.pays().then((pays)=>{
      console.log(pays);
      this.pays = pays.data;
    })
  }

  getVille() {
    this.utilService.makeLoader('Un instant svp!').then((loader)=>{
      loader.present();
      this.getInformationService.ville().then((villes)=>{
        console.log(villes);
        this.villes = villes.data;
        loader.dismiss();
      }).catch((error)=>{
        loader.dismiss();
      })
    });
  }

  search(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.expertService.search(this.data.ville_id,this.data.domaine_competence).then((experts)=>{
        console.log(experts.data.data);
        loader.dismiss();
        if (experts.data.data.length > 0) {
          this.router.navigate(['/expert-more'],{
            queryParams: {
               data : JSON.stringify(experts.data)
              },
            });
        }
        else{
          this.utilService.makeToast("Aucun expert ne répond à vos critères de recherches!").then((toast) =>{
            toast.present();
          })
        }
      })
      .catch((error)=>{
        console.log(error);
        loader.dismiss();
      })
    })
  }

}
