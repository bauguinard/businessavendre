import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExpertSearchPageRoutingModule } from './expert-search-routing.module';

import { ExpertSearchPage } from './expert-search.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpertSearchPageRoutingModule
  ],
  declarations: [ExpertSearchPage]
})
export class ExpertSearchPageModule {}
