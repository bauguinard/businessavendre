import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpertSearchPage } from './expert-search.page';

const routes: Routes = [
  {
    path: '',
    component: ExpertSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpertSearchPageRoutingModule {}
