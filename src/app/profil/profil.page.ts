import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { UtilsService } from '../services/utils.service';
import { menuController } from '@ionic/core';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {

  user:any = {

  };

  constructor(private utilService: UtilsService,private storage: Storage) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    menuController.close();
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.storage.get('user').then((data) => {
        console.log(data);
        this.user = data;
        console.log(this.user);
        loader.dismiss();
      });
    });
  }

}
