import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { UtilsService } from '../services/utils.service';
import { AuthService } from "../services/auth.service";
import { Router } from '@angular/router';
import { GetInformationService } from '../services/get-information.service';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.page.html',
  styleUrls: ['./user-update.page.scss'],
})
export class UserUpdatePage implements OnInit {
  pays:any = [];
  villes:any = [];
  data:any = {
    username:'',
    email:'',
    telephone:'',
    adresse:'',
    civilite:'',
    date_naissance:'',
    password:'',
    password_confirmation:'',
    type_id:'',
    pays_id:'',
    ville_id:''
  };
  constructor(private router: Router,
    private utilService: UtilsService,
    private storage: Storage,
    private authService: AuthService,
    private getInformationService: GetInformationService,) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.storage.get('user').then((user) => {
        console.log(user);
        this.data.adresse = user.adresse;
        this.data.civilite = user.civilite;
        this.data.username = user.username;
        this.data.societe = user.societe;
        this.data.raison_sociale = user.raison_sociale;
        this.data.email = user.email;
        this.data.telephone = user.telephone;
        this.data.date_naissance = user.date_naissance;
        this.data.type_id = user.type_id;
        this.data.pays_id = user.pays_id;
        this.data.ville_id = user.ville_id;
        this.data.id = user.id;

        this.getPays();
        this.getVille();
        loader.dismiss();
      });
    });
  }

  getPays() {
    this.getInformationService.pays().then((pays)=>{
      console.log(pays);
      this.pays = pays.data;
    })
  }

  getVille() {
    this.getInformationService.ville().then((villes)=>{
      console.log(villes);
      this.villes = villes.data;
    })
  }

  save(){
    this.utilService.makeLoader("Un instant svp!").then((loader)=>{
      loader.present();
      this.authService.update(this.data).then((response) => {
        loader.dismiss();
        console.log(response);
        this.utilService.makeToast("Votre profil a bien été modifié").then((toast)=>{
          toast.present();
          this.storage.remove('user').then(()=>{
            console.log(response.data)
            this.storage.set('user',response.data).then(()=>{
              this.router.navigate(['/profil']);
            })
          });
        })
      }).catch((error)=>{
        loader.dismiss();
        console.log(error);
        this.utilService.makeAlert('Business à Vendre',error).then((alert) =>{
          alert.present();
        })
      });
    })
  }

}
