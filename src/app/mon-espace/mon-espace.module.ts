import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MonEspacePageRoutingModule } from './mon-espace-routing.module';

import { MonEspacePage } from './mon-espace.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MonEspacePageRoutingModule
  ],
  declarations: [MonEspacePage]
})
export class MonEspacePageModule {}
