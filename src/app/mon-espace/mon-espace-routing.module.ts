import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonEspacePage } from './mon-espace.page';

const routes: Routes = [
  {
    path: '',
    component: MonEspacePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonEspacePageRoutingModule {}
