import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-mon-espace',
  templateUrl: './mon-espace.page.html',
  styleUrls: ['./mon-espace.page.scss'],
})
export class MonEspacePage implements OnInit {
  type_user:any = 0;
  constructor(private storage: Storage) { 
    this.storage.get('user').then((data) => {
      this.type_user = data.type_id;
    });
  }

  ngOnInit() {
  }

}
