import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MonEspacePage } from './mon-espace.page';

describe('MonEspacePage', () => {
  let component: MonEspacePage;
  let fixture: ComponentFixture<MonEspacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonEspacePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MonEspacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
