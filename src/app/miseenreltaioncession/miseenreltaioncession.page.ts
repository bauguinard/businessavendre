import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsService } from '../services/utils.service';
import { RelationService } from '../services/relation.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-miseenreltaioncession',
  templateUrl: './miseenreltaioncession.page.html',
  styleUrls: ['./miseenreltaioncession.page.scss'],
})
export class MiseenreltaioncessionPage implements OnInit {

  data:any = {
    nom:"",
    prenoms:"",
  };

  type:any;

  relation:any = {
    demandeur_id: "",
    receveur_id: "",
    nom:"",
    prenoms:"",
		attachable_type: "annonce cession",
		attachable_id: 1
  };

  relationInvestisseur:any = {
    demandeur_id: "",
    receveur_id: "",
		attachable_type: "annonce investisseur",
		attachable_id: 2
  };

  relationExpert:any = {
    demandeur_id: "",
    receveur_id: "",
		//attachable_type: "annonce investisseur",
		//attachable_id: 2
  };
  constructor(private route: ActivatedRoute,
    private utilService: UtilsService,
    private relationService: RelationService,
    private router: Router,
    private storage: Storage) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.route.params.subscribe(params => {
      console.log(params);
      this.type = params['type'];
      if(params['type'] == 1){
        this.relation.demandeur_id = params['demandeur_id'];
        this.relation.receveur_id = params['receveur_id'];
      }
      else if(params['type'] == 2){
        this.relationInvestisseur.demandeur_id = params['demandeur_id'];
        this.relationInvestisseur.receveur_id = params['receveur_id'];
      }
      else{
        this.relationExpert.demandeur_id = params['demandeur_id'];
        this.relationExpert.receveur_id = params['receveur_id'];
      }
    });
  }

  valider(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
    loader.present();
    if(this.type == 1){
      this.relation.nom = this.data.nom;
      this.relation.prenoms = this.data.prenoms;
      console.log(this.relation);
      this.relationService.miseEnRelation(this.relation).then((response)=>{
        loader.dismiss();
        console.log(response);
        if (response.message != "created") {
          this.utilService.makeToast(response.message).then((toast)=>{
            toast.present();
            if (response.message == "Désolé, ce demandeur n'a pas de soucription valide !") {
              this.router.navigate(['/abonnement']);
            }
            else{
              this.router.navigate(['/tchat-list-demandeur']);
            }
          })
        }
        else{
          this.utilService.makeToast("Votre demande de mise en relation a bien été envoyée, elle est en attente de validation!").then((toast)=>{
            toast.present();
            this.router.navigate(['/tchat-list-demandeur']);
          })
        }
        
      }).catch((error)=>{
        loader.dismiss();
        console.log(error);
        this.utilService.makeToast("Nous n'avons pas pu vous en relation veuillez réessayer plus tard!").then((toast)=>{
          toast.present();
        })
      })
    }
    else if(this.type == 2){
      this.relationInvestisseur.nom = this.data.nom;
      this.relationInvestisseur.prenoms = this.data.prenoms;
      console.log(this.relationInvestisseur);
      this.relationService.miseEnRelation(this.relationInvestisseur).then((response)=>{
        loader.dismiss();
        console.log(response);
        if (response.message != "created") {
          this.utilService.makeToast(response.message).then((toast)=>{
            toast.present();
            if (response.message == "Désolé, ce demandeur n'a pas de soucription valide !") {
              this.router.navigate(['/abonnement']);
            }
            else{
              this.router.navigate(['/tchat-list-demandeur']);
            }
            
          })
        }
        else{
          this.utilService.makeToast("Votre demande de mise en relation a bien été envoyée, elle est en attente de validation!").then((toast)=>{
            toast.present();
            this.router.navigate(['/tchat-list-demandeur']);
          })
        }
        
      }).catch((error)=>{
        loader.dismiss();
        console.log(error);
        this.utilService.makeToast("Nous n'avons pas pu vous en relation veuillez réessayer plus tard!").then((toast)=>{
          toast.present();
        })
      })
    }
    else{
      this.relationExpert.nom = this.data.nom;
      this.relationExpert.prenoms = this.data.prenoms;
      console.log(this.relationExpert);
      this.relationService.miseEnRelation(this.relationExpert).then((response)=>{
        loader.dismiss();
        console.log(response);
        if (response.message != "created") {
          this.utilService.makeToast(response.message).then((toast)=>{
            toast.present();
            if (response.message == "Désolé, ce demandeur n'a pas de soucription valide !") {
              //this.storage.set('noabonnement',1).then((success)=>{
                this.router.navigate(['/abonnement']);
              //});
            }
            else{
              this.router.navigate(['/tchat-list-demandeur']);
            }
          })
        }
        else{
          this.utilService.makeToast("Votre demande de mise en relation a bien été envoyée, elle est en attente de validation!").then((toast)=>{
            toast.present();
            this.router.navigate(['/tchat-list-demandeur']);
          })
        }
        
      }).catch((error)=>{
        loader.dismiss();
        console.log(error);
        this.utilService.makeToast("Nous n'avons pas pu vous en relation veuillez réessayer plus tard!").then((toast)=>{
          toast.present();
        })
      })
    }
    })
  }

}
