import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MiseenreltaioncessionPage } from './miseenreltaioncession.page';

describe('MiseenreltaioncessionPage', () => {
  let component: MiseenreltaioncessionPage;
  let fixture: ComponentFixture<MiseenreltaioncessionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiseenreltaioncessionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MiseenreltaioncessionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
