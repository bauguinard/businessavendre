import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MiseenreltaioncessionPageRoutingModule } from './miseenreltaioncession-routing.module';

import { MiseenreltaioncessionPage } from './miseenreltaioncession.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MiseenreltaioncessionPageRoutingModule
  ],
  declarations: [MiseenreltaioncessionPage]
})
export class MiseenreltaioncessionPageModule {}
