import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CedentMorePage } from './cedent-more.page';

const routes: Routes = [
  {
    path: '',
    component: CedentMorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CedentMorePageRoutingModule {}
