import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CedentMorePage } from './cedent-more.page';

describe('CedentMorePage', () => {
  let component: CedentMorePage;
  let fixture: ComponentFixture<CedentMorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CedentMorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CedentMorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
