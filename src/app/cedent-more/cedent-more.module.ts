import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CedentMorePageRoutingModule } from './cedent-more-routing.module';

import { CedentMorePage } from './cedent-more.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CedentMorePageRoutingModule
  ],
  declarations: [CedentMorePage]
})
export class CedentMorePageModule {}
