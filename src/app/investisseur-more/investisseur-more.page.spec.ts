import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InvestisseurMorePage } from './investisseur-more.page';

describe('InvestisseurMorePage', () => {
  let component: InvestisseurMorePage;
  let fixture: ComponentFixture<InvestisseurMorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestisseurMorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InvestisseurMorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
