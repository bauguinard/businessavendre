import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvestisseurMorePageRoutingModule } from './investisseur-more-routing.module';

import { InvestisseurMorePage } from './investisseur-more.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvestisseurMorePageRoutingModule
  ],
  declarations: [InvestisseurMorePage]
})
export class InvestisseurMorePageModule {}
