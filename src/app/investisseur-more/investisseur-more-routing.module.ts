import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvestisseurMorePage } from './investisseur-more.page';

const routes: Routes = [
  {
    path: '',
    component: InvestisseurMorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvestisseurMorePageRoutingModule {}
