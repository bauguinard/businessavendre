import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActualiteService } from '../services/actualite.service';
import { UtilsService } from '../services/utils.service';

@Component({
  selector: 'app-actualite-detail',
  templateUrl: './actualite-detail.page.html',
  styleUrls: ['./actualite-detail.page.scss'],
})
export class ActualiteDetailPage implements OnInit {
  public actualite = {
    title: "",
    content: "",
    content_html: "",
    created_at: "",
  };
  public actualites:any;

  constructor(private actualiteService: ActualiteService, private utilService: UtilsService,private route: ActivatedRoute) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.route.params.subscribe(params => {
      this.getDetailActualite(params.id);
    });
  }

  getDetailActualite(id){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.actualiteService.getDetailActualite(id).then((response)=>{
        console.log(response.data);
        this.actualites = response.data;
        let dateMonth = this.actualites.published_at;
        let dateArray = dateMonth.split(" ");
        let year = dateArray[0].split("-")[2];
        let month = dateArray[0].split("-")[1];
        let day = dateArray[0].split("-")[0];
        this.actualite.title = this.actualites.title;
        this.actualite.content = this.actualites.content;
        this.actualite.content_html = this.actualites.content_html;
        this.actualite.created_at = year+"/"+month+"/"+day;
        loader.dismiss();
      })
      .catch((err)=>{
        loader.dismiss();
      })
    })
  }

}
