import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreerAnnonceCedentPage } from './creer-annonce-cedent.page';

const routes: Routes = [
  {
    path: '',
    component: CreerAnnonceCedentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreerAnnonceCedentPageRoutingModule {}
