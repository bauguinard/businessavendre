import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreerAnnonceCedentPageRoutingModule } from './creer-annonce-cedent-routing.module';

import { CreerAnnonceCedentPage } from './creer-annonce-cedent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreerAnnonceCedentPageRoutingModule
  ],
  declarations: [CreerAnnonceCedentPage]
})
export class CreerAnnonceCedentPageModule {}
