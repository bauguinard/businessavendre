import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from "../services/utils.service";
import { GetInformationService } from "../services/get-information.service";
import { Storage } from '@ionic/storage';
import { Router } from "@angular/router";
import {IonSlides} from '@ionic/angular';
import { AnnonceCedantService } from '../services/annonce-cedant.service';

@Component({
  selector: 'app-creer-annonce-cedent',
  templateUrl: './creer-annonce-cedent.page.html',
  styleUrls: ['./creer-annonce-cedent.page.scss'],
})
export class CreerAnnonceCedentPage implements OnInit {
  @ViewChild(IonSlides,{static:true}) slides: IonSlides;
  data:any = {
    nom_entreprise: '',
    date_creation_entreprise: '',
    chiffre_affaire: '',
    raison_transaction: '',
    pourcentage_a_ceder: '',
    profil_partanaire: '',
    typesociete: '',
    pays: '',
    ville: '',
    typetransaction_id: '',
    user: '',
    secteuractivites:'',
    categorie_id:2,
    statut: 1,
  };

 
  categories:any;
  typetransactions:any;
  statuts:any;
  villes:any;
  pays:any;
  zones:any;
  typeSocietes:any;
  secteurs:any;

  constructor(private getInformationService: GetInformationService,
              private annonceCedantService:AnnonceCedantService,
              private utilService: UtilsService,
              private storage: Storage,
              private router: Router)
  {
      this.utilService.makeLoader("Patientez un instant svp!").then((loader)=>{
        loader.present();
        this.storage.get('user').then((user) => {
          this.data.user = user.id;
        });
        loader.dismiss();
      })
   }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.getData();
  }

  getData() {
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
    loader.present();
    this.getInformationService.pays().then((pays)=>{
      console.log(pays);
      this.pays = pays.data;
      this.getInformationService.ville().then((villes)=>{
        console.log(villes);
        this.villes = villes.data;
        this.annonceCedantService.getSecteurActivite().then((secteur)=>{
          console.log(secteur);
          this.secteurs = secteur.data;
          this.getInformationService.typeTransaction().then((typetransactions)=>{
            console.log(typetransactions);
            this.typetransactions = typetransactions.data;
            this.getInformationService.typeSociete().then((societes)=>{
              console.log(societes);
              this.typeSocietes = societes.data;
              loader.dismiss();
            })
          })
        }).catch((error)=>{
          console.error(error);
        })
      })
    })
    });
  }

  send(){
    console.log(this.data.title);
    if (this.data.typesociete_id != "" || this.data.nom_entreprise != "" || this.data.chiffre_affaire != ""|| this.data.secteuractivites != "")
    {
      this.data.date_creation_entreprise = this.data.date_creation_entreprise.split("T")[0];

      console.log(this.data);
      this.utilService.makeLoader("Patientez un instant svp!").then((loader)=>{
        loader.present();
        this.annonceCedantService.sendAnnonce(this.data).then((response)=>{
          loader.dismiss();
          console.log(response);
          this.utilService.makeToast("Votre annonce a bien été publiée!").then((toast)=>{
            toast.present().then(()=>{
              this.data = {
                nom_entreprise: '',
                date_creation_entreprise: '',
                chiffre_affaire: '',
                raison_transaction: '',
                pourcentage_a_ceder: '',
                profil_partanaire: '',
                typesociete: '',
                pays: '',
                ville: '',
                typetransaction_id: '',
                user: '',
                secteuractivites:'',
                categorie_id:2,
                statut: 1,
              };
            });
            this.router.navigateByUrl('/list-annonce-cedent');
          }).catch((error)=>{
            loader.dismiss();
            console.log(error);
            this.utilService.makeAlert("Business à vendre","Votre annonce n'a pu être publiée").then((alert)=>{
              alert.present();
            })
          })
        })
        .catch((err)=>{
          console.log(err);
          loader.dismiss();
          this.utilService.makeToast("Veuillez bien renseigner les champs svp!").then((toast)=>{
            toast.present();
          });
        })
      })
    }
    else
    {
      this.utilService.makeToast("Veuillez bien renseigner les champs svp!").then((toast)=>{
        toast.present();
      })
    }

  }


}
