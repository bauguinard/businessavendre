import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreerAnnonceCedentPage } from './creer-annonce-cedent.page';

describe('CreerAnnonceCedentPage', () => {
  let component: CreerAnnonceCedentPage;
  let fixture: ComponentFixture<CreerAnnonceCedentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreerAnnonceCedentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreerAnnonceCedentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
