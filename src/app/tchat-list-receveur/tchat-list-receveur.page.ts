import { Component, OnInit } from '@angular/core';
import { MessagerieService } from "../services/messagerie.service";
import { UtilsService } from '../services/utils.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { RelationService } from "../services/relation.service";
import { ConstvariableService } from "../services/constvariable.service";

@Component({
  selector: 'app-tchat-list-receveur',
  templateUrl: './tchat-list-receveur.page.html',
  styleUrls: ['./tchat-list-receveur.page.scss'],
})
export class TchatListReceveurPage implements OnInit {
  relations:any = [];
  user:any;
  relation_id:any = ""

  constructor(private messagerieService: MessagerieService,
    private storage: Storage,
    private router: Router,
    private utilService: UtilsService,
    private relationService: RelationService,
    private constVariable: ConstvariableService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((data) => {
      console.log(data);
      if(data.id != "")
      {
          this.user = data.id
          this.relationService.getDemandeRelationReception(this.user).then((relation)=>{
            this.relations = relation.data;

            console.log(relation.data);
          })
          .catch((error)=>{
            console.log(error);
          })
      }
    });
  }

  tchat(relation){
    if (relation.statut_id == 1) {
      this.relationService.getRelationInfo(relation.id)
      .then((info)=>{
        this.relation_id = info.data.id;
        console.log(info.data.id);
        this.router.navigate(['/tchat',this.relation_id]);
      })
      .catch((error)=>{
        console.log(error);
      });
    }
    else{
      this.utilService.makeToast("Votre demande n'a pas encore été validée, veuillez patientez que l'utilisateur valide votre demande").then((toast)=>{
        toast.present();
      });
    }
  }

  validerDemande(id){
    this.relationService.validerDemande(id)
    .then((response)=>{
      console.log(response.data);
      this.relation_id = response.data.id;
      this.utilService.makeToast(response.message).then((toast)=>{
        toast.present();
        this.storage.get('user').then((data) => {
          if(data.id != "")
          {
              this.user = data.id
              this.relationService.getDemandeRelationDemandeur(this.user).then((relation)=>{
                this.relations = relation.data;
    
                console.log(relation.data);
              })
              .catch((error)=>{
                console.log(error);
              })
          }
        });
      }) 
    })
    .catch((error)=>{
      console.log(error);
    })
  }
}
