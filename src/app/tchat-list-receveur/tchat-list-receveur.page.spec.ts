import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TchatListReceveurPage } from './tchat-list-receveur.page';

describe('TchatListReceveurPage', () => {
  let component: TchatListReceveurPage;
  let fixture: ComponentFixture<TchatListReceveurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TchatListReceveurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TchatListReceveurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
