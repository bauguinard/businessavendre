import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TchatListReceveurPage } from './tchat-list-receveur.page';

const routes: Routes = [
  {
    path: '',
    component: TchatListReceveurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TchatListReceveurPageRoutingModule {}
