import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TchatListReceveurPageRoutingModule } from './tchat-list-receveur-routing.module';

import { TchatListReceveurPage } from './tchat-list-receveur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TchatListReceveurPageRoutingModule
  ],
  declarations: [TchatListReceveurPage]
})
export class TchatListReceveurPageModule {}
