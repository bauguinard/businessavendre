import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilsService } from '../services/utils.service';
import { AnnonceCedantService } from "../services/annonce-cedant.service";
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cedent-detail',
  templateUrl: './cedent-detail.page.html',
  styleUrls: ['./cedent-detail.page.scss'],
})
export class CedentDetailPage implements OnInit, OnDestroy {
  id: number;
  private sub: any;
  user_id;
  myAnnonce:any ={
    id: "",
    nom_entreprise: "",
    date_creation_entreprise: "",
    chiffre_affaire: "1000000",
    raison_transaction: "",
    pourcentage_a_ceder: "",
    profil_partanaire: "",
    date_indicateur_fin: "",
    capitaux_propres: "",
    tresorerie_nette: "",
    dettes_financieres: "",
    active: "",
    typesociete_id: "",
    pays_id: "",
    ville_id: "",
    typetransaction_id: "",
    user_id: "",
    statut_id: "",
    categorie_id: "",
    created_at: "",
    updated_at: "",
    deleted_at: "",
    typesociete: {
      id: "",
      name: "",
      description: "",
      active: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    },
    user: {
      id: "",
      name: "",
      email: "",
      permissions: "",
      is_activated: "",
      activated_at: "",
      last_login: "",
      created_at: "",
      updated_at: "",
      username: "",
      surname: "",
      deleted_at: "",
      last_seen: "",
      is_guest: "",
      is_superuser: "",
      civilite: "",
      date_naissance: "",
      telephone: "",
      adresse: "",
      nombre_relation_restante: "",
      date_fin_abonnement: "",
      type_id: "",
      pays_id: "",
      ville_id: ""
    },
    statut: {
      id: "",
      name: "",
      description: "",
      active: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    },
    categorie: {
      id: "",
      name: "",
      description: "",
      active: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    },
    pays: {
      id: "",
      name: "",
      zip: "",
      indicatif: "",
      description: "",
      active: "",
      area_id: "",
      division_id: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    },
    ville: {
      id: "",
      name: "",
      zip: "",
      indicatif: "",
      description: "",
      active: "",
      area_id: "",
      division_id: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    },
    typetransaction: {
      id: "",
      name: "",
      description: "",
      active: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    }
  };
  retour:any = {
    status_code:"",
    message:"",
    data:""
  };


  constructor(private route: ActivatedRoute,
    private utilService: UtilsService,
    private router: Router,
    private storage: Storage,
    private annonceCedantService: AnnonceCedantService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  ionViewWillEnter() {
    this.storage.get('user').then((data) => {
      console.log(data);
      console.log(typeof data);
      
      if(data.id != "")
      {
        this.user_id = data.id
      }
    });

    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      console.log(this.id);
      this.annonceCedantService.getMyDetailAnnonce(this.id).then((response)=>{
        this.myAnnonce = response.data;
        console.log(response.data);
        console.log(this.myAnnonce);
        loader.dismiss();
      })
      .catch(()=>{
        loader.dismiss();
      })
    })
  }

  remove(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      console.log(this.id);
      this.annonceCedantService.removeAnnonce(this.id).then(()=>{
        loader.dismiss();
        this.router.navigate(['/cedant-detail',this.id]);
      })
    })
  }

  mise_en_relation(){
    this.storage.set('pathPage',['/cedent-detail',this.id]).then((success)=>{
      this.router.navigate(['/miseenreltaioncession', {
        'demandeur_id':this.user_id,
        'receveur_id':this.myAnnonce.user.id,
        'type':1
      }]);
    })
  }

  numStr(a, b) {
    a = '' + a;
    b = b || ' ';
    var c = '',
        d = 0;
    while (a.match(/^0[0-9]/)) {
      a = a.substr(1);
    }
    for (var i = a.length-1; i >= 0; i--) {
      c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
      d++;
    }
    return c;
  }

}
