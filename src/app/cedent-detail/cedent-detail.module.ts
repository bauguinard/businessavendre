import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CedentDetailPageRoutingModule } from './cedent-detail-routing.module';

import { CedentDetailPage } from './cedent-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CedentDetailPageRoutingModule
  ],
  declarations: [CedentDetailPage]
})
export class CedentDetailPageModule {}
