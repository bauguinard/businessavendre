import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CedentDetailPage } from './cedent-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CedentDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CedentDetailPageRoutingModule {}
