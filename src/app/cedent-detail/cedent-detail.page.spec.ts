import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CedentDetailPage } from './cedent-detail.page';

describe('CedentDetailPage', () => {
  let component: CedentDetailPage;
  let fixture: ComponentFixture<CedentDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CedentDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CedentDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
