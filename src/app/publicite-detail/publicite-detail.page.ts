import { Component, OnInit } from '@angular/core';
import { ActualiteService } from '../services/actualite.service';
import { UtilsService } from '../services/utils.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-publicite-detail',
  templateUrl: './publicite-detail.page.html',
  styleUrls: ['./publicite-detail.page.scss'],
})
export class PubliciteDetailPage implements OnInit {
  public publicite = {
    created_at:'',
    title:'',
    content:''
  };
  publicites:any;

  constructor(private actualiteService: ActualiteService,
    private utilService: UtilsService,
    private route: ActivatedRoute) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.route.params.subscribe(params => {
      console.log(params.id);
      this.getDetailActualite(params.id);
    });
  }

  getDetailActualite(id){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.actualiteService.getDetailActualite(id).then((response)=>{
        console.log(response.data);
        this.publicites = response.data;
        let dateMonth = this.publicites.published_at;
        let dateArray = dateMonth.split(" ");
        let year = dateArray[0].split("-")[2];
        let month = dateArray[0].split("-")[1];
        let day = dateArray[0].split("-")[0];
        this.publicite.title = this.publicites.title;
        this.publicite.content = this.publicites.content;
        this.publicite.created_at = year+"/"+month+"/"+day;
        loader.dismiss();
      })
      .catch((err)=>{
        loader.dismiss();
      })
    })
  }
}
