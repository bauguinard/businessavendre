import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PubliciteDetailPageRoutingModule } from './publicite-detail-routing.module';

import { PubliciteDetailPage } from './publicite-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PubliciteDetailPageRoutingModule
  ],
  declarations: [PubliciteDetailPage]
})
export class PubliciteDetailPageModule {}
