import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PubliciteDetailPage } from './publicite-detail.page';

describe('PubliciteDetailPage', () => {
  let component: PubliciteDetailPage;
  let fixture: ComponentFixture<PubliciteDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubliciteDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PubliciteDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
