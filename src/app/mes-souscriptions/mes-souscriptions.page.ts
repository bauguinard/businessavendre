import { Component, OnInit } from '@angular/core';
import { AbonnementService } from "../services/abonnement.service";
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mes-souscriptions',
  templateUrl: './mes-souscriptions.page.html',
  styleUrls: ['./mes-souscriptions.page.scss'],
})
export class MesSouscriptionsPage implements OnInit {
  abonnements = [];

  constructor(private abonnementService: AbonnementService,
    private router: Router,
    private storage: Storage) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((user)=>{
      this.abonnementService.getMySouscription(user.id).then((abonnement)=>{
        this.abonnements = abonnement.data;
        console.log(abonnement.data);
      });
    })
  }

  abonnement(){
    this.storage.set('pathPage',['/mes-souscriptions']).then((success)=>{
      this.router.navigate(['/abonnement']);
    });
  }

}
