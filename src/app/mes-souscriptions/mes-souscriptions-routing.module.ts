import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MesSouscriptionsPage } from './mes-souscriptions.page';

const routes: Routes = [
  {
    path: '',
    component: MesSouscriptionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MesSouscriptionsPageRoutingModule {}
