import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MesSouscriptionsPageRoutingModule } from './mes-souscriptions-routing.module';

import { MesSouscriptionsPage } from './mes-souscriptions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MesSouscriptionsPageRoutingModule
  ],
  declarations: [MesSouscriptionsPage]
})
export class MesSouscriptionsPageModule {}
