import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SignMenuPage } from './sign-menu.page';

describe('SignMenuPage', () => {
  let component: SignMenuPage;
  let fixture: ComponentFixture<SignMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignMenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SignMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
