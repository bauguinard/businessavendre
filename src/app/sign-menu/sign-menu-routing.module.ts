import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignMenuPage } from './sign-menu.page';

const routes: Routes = [
  {
    path: '',
    component: SignMenuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignMenuPageRoutingModule {}
