import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignMenuPageRoutingModule } from './sign-menu-routing.module';

import { SignMenuPage } from './sign-menu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignMenuPageRoutingModule
  ],
  declarations: [SignMenuPage]
})
export class SignMenuPageModule {}
