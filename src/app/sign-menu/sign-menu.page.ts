import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from "@angular/router";
@Component({
  selector: 'app-sign-menu',
  templateUrl: './sign-menu.page.html',
  styleUrls: ['./sign-menu.page.scss'],
})
export class SignMenuPage implements OnInit {

  constructor(private storage: Storage,private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((data) => {
      if (data) {
        if (data.id != "") {
          this.router.navigate(['/home']);
        }
      }
    });
  }


}
