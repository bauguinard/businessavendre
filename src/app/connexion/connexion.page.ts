import { Component, OnInit } from '@angular/core';
import { UtilsService } from "../services/utils.service";
import { AuthService } from "../services/auth.service";
import { Storage } from '@ionic/storage';
import { menuController } from '@ionic/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {
  data:any = {
    login:'',
    password:'',
  };

  constructor(private utilService: UtilsService,
    private authService: AuthService,
    private storage: Storage,
    private router: Router,
    private appComponent: AppComponent) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    menuController.close();
  }

  signIn(){
    let loader = this.utilService.makeLoader("Authentification...");
    loader.then((data)=>{
      data.present();
      this.authService.login(this.data).then((response)=>{
        console.log(response.user);
        this.storage.set('user',response.user).then(()=>{
          this.appComponent.connecte = true;
          data.dismiss();
          this.router.navigate(['/home']);
        })
      }).catch((error)=>{
        console.log(error);
        data.dismiss();
        let alert = this.utilService.makeAlert('Business à vendre',"Vos coordonnées ne sont pas correctes, veuillez réessayer svp!");
        alert.then((dat)=>dat.present());
      });
    });
  }

}
