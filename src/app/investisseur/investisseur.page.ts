import { Component, OnInit } from '@angular/core';
import { AnnonceInvestisseurService } from '../services/annonce-investisseur.service';
import { UtilsService } from '../services/utils.service';

@Component({
  selector: 'app-investisseur',
  templateUrl: './investisseur.page.html',
  styleUrls: ['./investisseur.page.scss'],
})
export class InvestisseurPage implements OnInit {
  user:any;
  myAnnonces:any;
  annonces:any = [];
  pages:any;
  currentPage = 1;

  constructor(private utilService: UtilsService,
    private annonceInvestisseurService: AnnonceInvestisseurService) {
      
    }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getAnnonce();
  }

  ionViewWillLeave() {
    this.annonces = [];
  }

  doRefresh(event){
    this.annonces = [];
    this.getAnnonce();
    event.target.complete();
  }

  getAnnonce(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.annonceInvestisseurService.getAllAnnonce().then((response)=>{
        console.log(response.data);
        this.myAnnonces = response.data;
        this.myAnnonces.forEach(annonce => {
          let dateMonth = annonce.created_at;
          let dateArray = dateMonth.split(" ");
          let year = dateArray[0].split("-")[2];
          let month = dateArray[0].split("-")[1];
          let myDate = year+"/"+month;

          let objetAnnonce = {
            id:annonce.id,
            created_at:myDate,
            activite:annonce.secteuractivites,
            ville:annonce.ville.name,
            montant_a_investir:annonce.fourchette_montant_a_investir
          };
          
          this.annonces.push(objetAnnonce);
        });
        console.log(this.annonces);
        loader.dismiss();
      })
    })
  }

  numStr(a, b) {
    a = '' + a;
    b = b || ' ';
    var c = '',
        d = 0;
    while (a.match(/^0[0-9]/)) {
      a = a.substr(1);
    }
    for (var i = a.length-1; i >= 0; i--) {
      c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
      d++;
    }
    return c;
  }
}
