import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvestisseurPageRoutingModule } from './investisseur-routing.module';

import { InvestisseurPage } from './investisseur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvestisseurPageRoutingModule
  ],
  declarations: [InvestisseurPage]
})
export class InvestisseurPageModule {}
