import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InvestisseurPage } from './investisseur.page';

describe('InvestisseurPage', () => {
  let component: InvestisseurPage;
  let fixture: ComponentFixture<InvestisseurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestisseurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InvestisseurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
