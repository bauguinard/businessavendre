import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FormationDetailPage } from './formation-detail.page';

describe('FormationDetailPage', () => {
  let component: FormationDetailPage;
  let fixture: ComponentFixture<FormationDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormationDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FormationDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
