import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilsService } from '../services/utils.service';
import { FormationService } from '../services/formation.service';

@Component({
  selector: 'app-formation-detail',
  templateUrl: './formation-detail.page.html',
  styleUrls: ['./formation-detail.page.scss'],
})
export class FormationDetailPage implements OnInit {
  sub: any;
  id: number;
  formation:any;
  formationDetail:any = {
    title: "",
    content: "",
    content_html: "",
    published_at: "",
  };


  constructor(private route: ActivatedRoute,
    private utilService: UtilsService,
    private formationService: FormationService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  ionViewWillEnter(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.getFormationDetail(this.id);
      loader.dismiss();
    })
  }

  getFormationDetail(id)
  {
    this.formationService.getFormationDetail(id).then((response)=>{
      console.log(response.data);
      this.formation = response.data;
      let dateMonth = this.formation.published_at;
      let dateArray = dateMonth.split(" ");
      let year = dateArray[0].split("-")[2];
      let month = dateArray[0].split("-")[1];
      let day = dateArray[0].split("-")[0];
      this.formationDetail.title = this.formation.title;
      this.formationDetail.content = this.formation.content;
      this.formationDetail.content_html = this.formation.content_html;
      this.formationDetail.published_at = year+"/"+month+"/"+day;
    })
  }

}
