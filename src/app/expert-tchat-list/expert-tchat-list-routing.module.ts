import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpertTchatListPage } from './expert-tchat-list.page';

const routes: Routes = [
  {
    path: '',
    component: ExpertTchatListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpertTchatListPageRoutingModule {}
