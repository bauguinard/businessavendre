import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExpertTchatListPageRoutingModule } from './expert-tchat-list-routing.module';

import { ExpertTchatListPage } from './expert-tchat-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpertTchatListPageRoutingModule
  ],
  declarations: [ExpertTchatListPage]
})
export class ExpertTchatListPageModule {}
