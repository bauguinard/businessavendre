import { Component, OnInit } from '@angular/core';
import { MessagerieService } from "../services/messagerie.service";
import { UtilsService } from '../services/utils.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-expert-tchat-list',
  templateUrl: './expert-tchat-list.page.html',
  styleUrls: ['./expert-tchat-list.page.scss'],
})
export class ExpertTchatListPage implements OnInit {
  relations:any;
  user:any;
  
  constructor(private messagerieService: MessagerieService,
    private storage: Storage,
    private router: Router,
    private utilService: UtilsService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((data) => {
      console.log(data);
      if(data.id != "")
      {
          this.user = data.id
          this.messagerieService.getRelation(data).then((relation)=>{
            console.log(relation.data);
            this.relations = relation.data;
            console.log(this.relations);
          })
          .catch((error)=>{
            console.log(error);
          })
      }
    });
  }

  tchat(expert){
    //verifier si l'utilisateur a un abonnement en cours
    this.messagerieService.verification(this.user).then((response)=>{
      console.log(response);
      if (response.message == "L'utilisateur peut être mis en relation !") {
        this.router.navigate(['/expert-tchat'],{
          queryParams: {
             expert : JSON.stringify(expert)
            },
          });
      }
      else{
        this.utilService.makeToast("Désolé vous n'avez pas d'abonnement actif pour echanger avec cet expert").then((toast)=>{
          toast.present();
          this.router.navigate(['/abonnement']);
        })
      }
    })
    .catch((error)=>{
      this.utilService.makeToast("Désolé vous n'avez pas d'abonnement actif pour echanger avec cet expert").then((toast)=>{
        toast.present();
        this.router.navigate(['/abonnement']);
      })
    })
  }

}
