import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExpertTchatListPage } from './expert-tchat-list.page';

describe('ExpertTchatListPage', () => {
  let component: ExpertTchatListPage;
  let fixture: ComponentFixture<ExpertTchatListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertTchatListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExpertTchatListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
