import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sign-menu',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'liste-formation',
    loadChildren: () => import('./liste-formation/liste-formation.module').then( m => m.ListeFormationPageModule)
  },
  {
    path: 'cedent',
    loadChildren: () => import('./cedent/cedent.module').then( m => m.CedentPageModule)
  },
  {
    path: 'investisseur',
    loadChildren: () => import('./investisseur/investisseur.module').then( m => m.InvestisseurPageModule)
  },
  {
    path: 'expert',
    loadChildren: () => import('./expert/expert.module').then( m => m.ExpertPageModule)
  },
  {
    path: 'abonnement',
    loadChildren: () => import('./abonnement/abonnement.module').then( m => m.AbonnementPageModule)
  },
  {
    path: 'cedent-detail/:id',
    loadChildren: () => import('./cedent-detail/cedent-detail.module').then( m => m.CedentDetailPageModule)
  },
  {
    path: 'expert-detail/:id',
    loadChildren: () => import('./expert-detail/expert-detail.module').then( m => m.ExpertDetailPageModule)
  },
  {
    path: 'investisseur-detail/:id',
    loadChildren: () => import('./investisseur-detail/investisseur-detail.module').then( m => m.InvestisseurDetailPageModule)
  },
  {
    path: 'cedent-more',
    loadChildren: () => import('./cedent-more/cedent-more.module').then( m => m.CedentMorePageModule)
  },
  {
    path: 'investisseur-more',
    loadChildren: () => import('./investisseur-more/investisseur-more.module').then( m => m.InvestisseurMorePageModule)
  },
  {
    path: 'expert-more',
    loadChildren: () => import('./expert-more/expert-more.module').then( m => m.ExpertMorePageModule)
  },
  {
    path: 'expert-search',
    loadChildren: () => import('./expert-search/expert-search.module').then( m => m.ExpertSearchPageModule)
  },
  {
    path: 'inscription',
    loadChildren: () => import('./inscription/inscription.module').then( m => m.InscriptionPageModule)
  },
  {
    path: 'connexion',
    loadChildren: () => import('./connexion/connexion.module').then( m => m.ConnexionPageModule)
  },
  {
    path: 'formation-detail/:id',
    loadChildren: () => import('./formation-detail/formation-detail.module').then( m => m.FormationDetailPageModule)
  },
  {
    path: 'creer-annonce-cedent',
    loadChildren: () => import('./creer-annonce-cedent/creer-annonce-cedent.module').then( m => m.CreerAnnonceCedentPageModule)
  },
  {
    path: 'creer-annonce-investisseur',
    loadChildren: () => import('./creer-annonce-investisseur/creer-annonce-investisseur.module').then( m => m.CreerAnnonceInvestisseurPageModule)
  },
  {
    path: 'list-annonce-cedent',
    loadChildren: () => import('./list-annonce-cedent/list-annonce-cedent.module').then( m => m.ListAnnonceCedentPageModule)
  },
  {
    path: 'list-annonce-investisseur',
    loadChildren: () => import('./list-annonce-investisseur/list-annonce-investisseur.module').then( m => m.ListAnnonceInvestisseurPageModule)
  },
  {
    path: 'mon-espace',
    loadChildren: () => import('./mon-espace/mon-espace.module').then( m => m.MonEspacePageModule)
  },
  {
    path: 'tchat/:id',
    loadChildren: () => import('./tchat/tchat.module').then( m => m.TchatPageModule)
  },
  {
    path: 'paiement/:id',
    loadChildren: () => import('./paiement/paiement.module').then( m => m.PaiementPageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./profil/profil.module').then( m => m.ProfilPageModule)
  },
  {
    path: 'user-update',
    loadChildren: () => import('./user-update/user-update.module').then( m => m.UserUpdatePageModule)
  },
  {
    path: 'actualite',
    loadChildren: () => import('./actualite/actualite.module').then( m => m.ActualitePageModule)
  },
  {
    path: 'publicite',
    loadChildren: () => import('./publicite/publicite.module').then( m => m.PublicitePageModule)
  },
  {
    path: 'actualite-detail/:id',
    loadChildren: () => import('./actualite-detail/actualite-detail.module').then( m => m.ActualiteDetailPageModule)
  },
  {
    path: 'publicite-detail/:id',
    loadChildren: () => import('./publicite-detail/publicite-detail.module').then( m => m.PubliciteDetailPageModule)
  },
  {
    path: 'expert-tchat',
    loadChildren: () => import('./expert-tchat/expert-tchat.module').then( m => m.ExpertTchatPageModule)
  },
  {
    path: 'expert-tchat-list',
    loadChildren: () => import('./expert-tchat-list/expert-tchat-list.module').then( m => m.ExpertTchatListPageModule)
  },
  {
    path: 'sign-menu',
    loadChildren: () => import('./sign-menu/sign-menu.module').then( m => m.SignMenuPageModule)
  },
  {
    path: 'paiement-expert',
    loadChildren: () => import('./paiement-expert/paiement-expert.module').then( m => m.PaiementExpertPageModule)
  },
  {
    path: 'cedent-search',
    loadChildren: () => import('./cedent-search/cedent-search.module').then( m => m.CedentSearchPageModule)
  },
  {
    path: 'investisseur-search',
    loadChildren: () => import('./investisseur-search/investisseur-search.module').then( m => m.InvestisseurSearchPageModule)
  },
  {
    path: 'tchat-list-receveur',
    loadChildren: () => import('./tchat-list-receveur/tchat-list-receveur.module').then( m => m.TchatListReceveurPageModule)
  },
  {
    path: 'tchat-list-demandeur',
    loadChildren: () => import('./tchat-list-demandeur/tchat-list-demandeur.module').then( m => m.TchatListDemandeurPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'mes-souscriptions',
    loadChildren: () => import('./mes-souscriptions/mes-souscriptions.module').then( m => m.MesSouscriptionsPageModule)
  },
  {
    path: 'miseenreltaioncession',
    loadChildren: () => import('./miseenreltaioncession/miseenreltaioncession.module').then( m => m.MiseenreltaioncessionPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
