import { Component } from '@angular/core';
import { UtilsService } from "../services/utils.service";
import { Storage } from '@ionic/storage';
import { AnnonceCedantService } from "../services/annonce-cedant.service";
import { AnnonceInvestisseurService } from "../services/annonce-investisseur.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  annoncesCedant:any;
  annoncesCedantFormat:any = [];
  annoncesInvestisseur:any;
  annoncesInvestisseurFormat:any = [];

  constructor(
    private utilService: UtilsService,
    private storage: Storage,
    private annonceCedantService: AnnonceCedantService,
    private router: Router,
    private annonceInvestisseurService: AnnonceInvestisseurService) {
      this.getUserInformation();
    }

  ionViewWillEnter() {
    this.annoncesInvestisseurFormat = [];
    this.annoncesCedantFormat = [];
    this.getAnnonces();
  }

  ionViewWillLeave() {
    this.annoncesInvestisseurFormat = [];
    this.annoncesCedantFormat = [];
  }

  doRefresh(event){
    this.annoncesInvestisseurFormat = [];
    this.annoncesCedantFormat = [];
    this.getAnnonces();
    event.target.complete();
  }

  getAnnonces(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.annonceCedantService.getLastAnnonceCedant().then((response)=>{
        this.annoncesCedant = response.data;
        this.annoncesCedant.forEach(annonce => {
          console.log(typeof annonce.ville);
          let dateMonth = annonce.created_at;
          let dateArray = dateMonth.split(" ");
          let year = dateArray[0].split("-")[2];
          let month = dateArray[0].split("-")[1];
          let myDate = year+"/"+month;

          let objetAnnonce = {
            id:annonce.id,
            created_at:myDate,
            chiffre_affaire:annonce.chiffre_affaire,
            activite:annonce.secteuractivites,
            ville:annonce.ville
          };
          
          this.annoncesCedantFormat.push(objetAnnonce);
          
        });
        console.log(this.annoncesCedantFormat);
        this.annonceInvestisseurService.getLastAnnonceInvestisseur().then((response)=>{
          this.annoncesInvestisseur = response.data;
          this.annoncesInvestisseur.forEach(annonce => {
            let dateMonth = annonce.created_at;
            let dateArray = dateMonth.split(" ");
            let year = dateArray[0].split("-")[2];
            let month = dateArray[0].split("-")[1];
            let myDate = year+"/"+month;
  
            let objetAnnonce = {
              id:annonce.id,
              created_at:myDate,
              activite:annonce.secteuractivites,
              ville:annonce.ville.name,
              montant_a_investir:annonce.fourchette_montant_a_investir
            };
            
            this.annoncesInvestisseurFormat.push(objetAnnonce);
          });
          console.log(this.annoncesInvestisseurFormat);
          loader.dismiss();
        }).catch((error)=>{
          console.log(error);
          this.annoncesInvestisseurFormat = [];
          loader.dismiss();
          this.utilService.makeAlert("Business à vendre","Votre connexion semble avoir un problème").then((alert)=>{
            alert.present();
          })
        })
      }).catch((error)=>{
        console.log(error);
        this.annoncesCedantFormat = [];
        loader.dismiss();
        this.utilService.makeAlert("Business à vendre","Votre connexion semble avoir un problème").then((alert)=>{
          alert.present();
        })
      })
    });
  }

  getUserInformation(){
    this.storage.get('user').then((data) => {
      if(typeof data == "object")
      {
        this.utilService.makeToast("Content de vous voir "+data.username,3000).then((toast)=>{
          toast.present();
        })
      }
    });
  }

  numStr(a, b) {
    a = '' + a;
    b = b || ' ';
    var c = '',
        d = 0;
    while (a.match(/^0[0-9]/)) {
      a = a.substr(1);
    }
    for (var i = a.length-1; i >= 0; i--) {
      c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
      d++;
    }
    return c;
  }

  logout(){
    this.storage.remove('user').then(()=>{
      this.router.navigate(['connexion']);
    });
  }
}
