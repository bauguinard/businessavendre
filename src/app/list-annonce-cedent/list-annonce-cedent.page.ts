import { Component, OnInit } from '@angular/core';
import { UtilsService } from "../services/utils.service";
import { Storage } from '@ionic/storage';
import { AnnonceCedantService } from "../services/annonce-cedant.service";

@Component({
  selector: 'app-list-annonce-cedent',
  templateUrl: './list-annonce-cedent.page.html',
  styleUrls: ['./list-annonce-cedent.page.scss'],
})
export class ListAnnonceCedentPage implements OnInit {
  user:any;
  myAnnonces:any;
  annonces:any = [];

  constructor(private utilService: UtilsService,
    private storage: Storage,
    private annonceCedantService: AnnonceCedantService) {
      
     }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.storage.get('user').then((data)=>{
      this.user = data.id;

      this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
        loader.present();
        this.annonceCedantService.getMyAnnonces(this.user).then((response)=>{
          console.log(response.data);
          this.myAnnonces = response.data;
          this.myAnnonces.forEach(annonce => {
            let dateMonth = annonce.created_at;
            let dateArray = dateMonth.split(" ");
            let year = dateArray[0].split("-")[2];
            let month = dateArray[0].split("-")[1];
            let myDate = year+"/"+month;
  
            let objetAnnonce = {
              id:annonce.id,
              created_at:myDate,
              chiffre_affaire:annonce.chiffre_affaire,
              activite:annonce.secteuractivites,
              ville:annonce.ville
            };
  
            this.annonces.push(objetAnnonce);
          });
          loader.dismiss();
        }).catch((error)=>{
          console.log(error);
          loader.dismiss();
          this.utilService.makeAlert("Business à vendre","Votre connexion semble avoir un problème").then((alert)=>{
            alert.present();
          })
        })
      })
    })
  }

  ionViewWillLeave() {
    this.annonces = [];
  }

  numStr(a, b) {
    a = '' + a;
    b = b || ' ';
    var c = '',
        d = 0;
    while (a.match(/^0[0-9]/)) {
      a = a.substr(1);
    }
    for (var i = a.length-1; i >= 0; i--) {
      c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
      d++;
    }
    return c;
  }

}
