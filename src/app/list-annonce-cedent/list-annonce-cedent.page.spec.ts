import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListAnnonceCedentPage } from './list-annonce-cedent.page';

describe('ListAnnonceCedentPage', () => {
  let component: ListAnnonceCedentPage;
  let fixture: ComponentFixture<ListAnnonceCedentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAnnonceCedentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListAnnonceCedentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
