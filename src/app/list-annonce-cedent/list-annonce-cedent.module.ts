import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListAnnonceCedentPageRoutingModule } from './list-annonce-cedent-routing.module';

import { ListAnnonceCedentPage } from './list-annonce-cedent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListAnnonceCedentPageRoutingModule
  ],
  declarations: [ListAnnonceCedentPage]
})
export class ListAnnonceCedentPageModule {}
