import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CedentSearchPageRoutingModule } from './cedent-search-routing.module';

import { CedentSearchPage } from './cedent-search.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CedentSearchPageRoutingModule
  ],
  declarations: [CedentSearchPage]
})
export class CedentSearchPageModule {}
