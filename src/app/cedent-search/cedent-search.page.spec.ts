import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CedentSearchPage } from './cedent-search.page';

describe('CedentSearchPage', () => {
  let component: CedentSearchPage;
  let fixture: ComponentFixture<CedentSearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CedentSearchPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CedentSearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
