import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CedentSearchPage } from './cedent-search.page';

const routes: Routes = [
  {
    path: '',
    component: CedentSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CedentSearchPageRoutingModule {}
