import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AbonnementService } from '../services/abonnement.service';
import { UtilsService } from '../services/utils.service';
import { Storage } from '@ionic/storage';
import { PaiementService } from "../services/paiement.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.page.html',
  styleUrls: ['./paiement.page.scss'],
})
export class PaiementPage implements OnInit {
  public offre = {
    id:"",
    title: "",
    description: "",
    duree: "",
    nombre_relation: "",
    cout: "",
    active: "",
    typeuser_id: "",
    statut_id: "",
    updated_at: "",
    created_at: "",
  };

  public data = {
    user_id: "",
	  offre_id: "",
	  montant: "",
    operateur: "",
    //contact:""
  }

  constructor(private router: Router,
    private paiementService: PaiementService,
    private storage: Storage,
    private abonnementService: AbonnementService,
    private utilService: UtilsService,
    private route: ActivatedRoute) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.route.params.subscribe(params => {
      console.log(params);
      this.getDetailOffre(params.id);
    });
  }

  getDetailOffre(id){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.abonnementService.getDetailOffre(id).then((response)=>{
        console.log(response);
        this.offre = response.data;
        this.data.offre_id = this.offre.id;
        this.data.montant = this.offre.cout;
        loader.dismiss();
      }).catch(()=>{
        loader.dismiss();
      })
    })
  }

  checkout(){
    this.utilService.makeLoader("Patientez un instant").then((loader)=>{
      loader.present();
      this.storage.get('user').then((user) => {
        if(user.id != ""){
          if (/*this.data.contact != "" && */this.data.operateur != "") {
            this.data.user_id = user.id;
            console.log(this.data);
            this.paiementService.paiement(this.data).then((response)=>{
              loader.dismiss();
              console.log(response);
              this.utilService.makeToast("Félicitation, Merci d'avoir souscrire à notre offre, elle expire le "+response.data.datefin+" !").then((toast)=>{
                toast.present();
                this.storage.get('pathPage').then((pathPage)=>{
                  console.log(pathPage);
                  if (pathPage.length == 1) {
                    this.router.navigate([pathPage[0]]);
                  }
                  else{
                    this.router.navigate([pathPage[0],pathPage[1]]);
                  }
                })
              })
            }).catch((error)=>{
              loader.dismiss();
              this.utilService.makeAlert("BAV","Votre solde est insuffisant!").then((alert)=>{
                console.log(error);
                alert.present();
              })
            })
          }
          else{
            loader.dismiss();
            this.utilService.makeToast("Veuillez renseigner le contact et l'opérateur svp!").then((toast)=>{
              toast.present();
            })
          }
        }
        else{
          loader.dismiss();
          this.utilService.makeToast("Veuillez vous connecter svp!").then((toast)=>{
            toast.present();
          })
        }
      })
    })

  }

}
