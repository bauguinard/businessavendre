import { Component, OnInit } from '@angular/core';
import { UtilsService } from "../services/utils.service";
import { AuthService } from "../services/auth.service";
import { Storage } from '@ionic/storage';
import { menuController } from '@ionic/core';
import { Router } from '@angular/router';
import { AppComponent } from "../app.component";
import { GetInformationService } from '../services/get-information.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {

  data:any = {
    username:'',
    email:'',
    telephone:'',
    adresse:'',
    password:'',
    password_confirmation:'',
    type_id:'',
    pays_id:'',
    ville_id:'',
    domaine_competence:''
  };

  pays:any = [];
  villes:any = [];

  constructor(private utilService: UtilsService,
              private authService: AuthService,
              private storage: Storage,
              private router: Router,
              private getInformationService: GetInformationService,
              private appComponent: AppComponent) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    menuController.close();
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.getInformationService.pays().then((pays)=>{
        console.log(pays);
        this.pays = pays.data;
        this.getInformationService.ville().then((villes)=>{
          console.log(villes);
          this.villes = villes.data;
          loader.dismiss();
        }).catch((err)=>{
          console.log(err);
        })
      })
      /*this.getPays();
      this.getVille();*/
    })
  }

  /*getPays() {
    
  }

  getVille() {
    
  }*/

  signUp(){
    if (this.data.type_id == "2") {
      this.storage.set('data',this.data).then(()=>{
        this.router.navigate(['/paiement-expert']);
      })
    }
    else{
      let loader = this.utilService.makeLoader("Inscription...");
      loader.then((data)=>{
        data.present();
        this.authService.register(this.data).then((response)=>{
          this.storage.set('user',response.data).then(()=>{
            this.appComponent.connecte = true;
            data.dismiss();
            this.router.navigate(['/home']);
          })
        }).catch((error)=>{
          console.log(error);
          data.dismiss();
          let alert = this.utilService.makeAlert('Business à vendre','Veuillez bien renseigner les champs svp!');
          alert.then((dat)=>dat.present());
        });
      });
    }
  }

}
