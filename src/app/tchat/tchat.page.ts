import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { UtilsService } from '../services/utils.service';
import { MessagerieService } from "../services/messagerie.service";
import { RelationService } from "../services/relation.service";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tchat',
  templateUrl: './tchat.page.html',
  styleUrls: ['./tchat.page.scss'],
})
export class TchatPage implements OnInit {
  data:any = {
    relation_id:"",
    demandeur_id:'',
    receveur_id:'',
    message:""
  };

  user:any = {
    id: "",
    name: "",
    email: "",
    permissions: "",
    is_activated: "",
    activated_at: "",
    last_login: "",
    created_at: "",
    updated_at: "",
    username: "",
    surname: "",
    deleted_at: "",
    last_seen: "",
    is_guest: "",
    is_superuser: "",
    civilite: "",
    date_naissance: "",
    telephone: "",
    adresse: "",
    nombre_relation_restante: "",
    date_fin_abonnement: "",
    type_id: "",
    pays_id: "",
  };
  user_id:any;
  messages:any = [];

  constructor(
    private utilService: UtilsService,
    private messagerieService: MessagerieService,
    private storage: Storage,
    private relationService:RelationService,
    private activeRoute:ActivatedRoute) { }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.utilService.makeLoader("Un instant svp...").then((loader)=>{
      loader.present();
      this.activeRoute.params.subscribe(params => {
        this.data.relation_id = params.id;
        console.log("relation_id "+this.data.relation_id);
        this.storage.get('user').then((data)=>{
          this.storage.get('relation').then((relation)=>{
            this.user_id = data.id;
            if (this.user_id == relation.demandeur.id) {
              this.data.demandeur_id = relation.demandeur.id;
              this.data.receveur_id = relation.receveur.id;    
              this.user = relation.receveur;
              loader.dismiss();
            }
            else{
              this.data.demandeur_id = relation.receveur.id;
              this.data.receveur_id = relation.demandeur.id;    
              this.user = relation.demandeur;
              loader.dismiss();
            }
          });
        });
      })
    })
  }

  send(){
    if (this.data.message == "") {
      this.utilService.makeToast("Veuillez entrer du texte svp!",1000).then((toast)=>{
        toast.present();
      })
    }
    else{
      console.log(this.data);
      this.messagerieService.send(this.data).then((response)=>{
        console.log(response);
        this.data.message  = "";
        this.messages = [];
        this.messagerieService.getMessage(this.data.relation_id).then((messages)=>{
          console.log(messages.data);
          this.messages = messages.data;
        })
        this.utilService.makeToast("Message envoyé",1000).then((toast)=>{
          toast.present();
        })
      }).catch((error)=>{
        console.log(error);
        this.utilService.makeToast("Message non envoyé",5000).then((toast)=>{
          toast.present();
        })
      })
    }
  }

  doRefresh(event){
    this.messagerieService.getMessage(this.data.relation_id).then((messages)=>{
      this.messages = [];
      this.messages = messages.data;
      event.target.complete();
    })
  }

}
