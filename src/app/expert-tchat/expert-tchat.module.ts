import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExpertTchatPageRoutingModule } from './expert-tchat-routing.module';

import { ExpertTchatPage } from './expert-tchat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpertTchatPageRoutingModule
  ],
  declarations: [ExpertTchatPage]
})
export class ExpertTchatPageModule {}
