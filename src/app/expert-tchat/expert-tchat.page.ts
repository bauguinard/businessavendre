import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessagerieService } from '../services/messagerie.service';
import { UtilsService } from '../services/utils.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-expert-tchat',
  templateUrl: './expert-tchat.page.html',
  styleUrls: ['./expert-tchat.page.scss'],
})
export class ExpertTchatPage implements OnInit {
  message:any;
  expert:any={
    societe:'',
    telephone:'',
    username:''
  };
  user:any ={
    id:'',
    type:''
  };

  messages:any;

  constructor(private activeRoute: ActivatedRoute,
    private storage: Storage,
    private messagerieService: MessagerieService,
    private utilService: UtilsService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((data) => {
      console.log(data);
      if(typeof data == "object")
      {
        this.activeRoute.queryParams.subscribe(params => {
          this.expert = JSON.parse(params['expert']);
          console.log(this.expert);
          /*this.messagerieService.getMessage(data).then((message)=>{
            console.log(message.data);
            this.messages = message.data;
            console.log(this.messages);
          })
          .catch((error)=>{
            console.log(error);
          })*/
        });
      }
    });
  }

  send(){

  }
}
