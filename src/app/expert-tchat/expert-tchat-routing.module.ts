import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpertTchatPage } from './expert-tchat.page';

const routes: Routes = [
  {
    path: '',
    component: ExpertTchatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpertTchatPageRoutingModule {}
