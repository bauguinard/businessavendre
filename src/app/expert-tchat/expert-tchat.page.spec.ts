import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExpertTchatPage } from './expert-tchat.page';

describe('ExpertTchatPage', () => {
  let component: ExpertTchatPage;
  let fixture: ComponentFixture<ExpertTchatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertTchatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExpertTchatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
