import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListAnnonceInvestisseurPage } from './list-annonce-investisseur.page';

const routes: Routes = [
  {
    path: '',
    component: ListAnnonceInvestisseurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListAnnonceInvestisseurPageRoutingModule {}
