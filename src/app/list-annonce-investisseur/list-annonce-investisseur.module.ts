import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListAnnonceInvestisseurPageRoutingModule } from './list-annonce-investisseur-routing.module';

import { ListAnnonceInvestisseurPage } from './list-annonce-investisseur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListAnnonceInvestisseurPageRoutingModule
  ],
  declarations: [ListAnnonceInvestisseurPage]
})
export class ListAnnonceInvestisseurPageModule {}
