import { Component, OnInit } from '@angular/core';
import { UtilsService } from "../services/utils.service";
import { Storage } from '@ionic/storage';
import { AnnonceInvestisseurService } from "../services/annonce-investisseur.service";
import { ConstvariableService } from '../services/constvariable.service';
@Component({
  selector: 'app-list-annonce-investisseur',
  templateUrl: './list-annonce-investisseur.page.html',
  styleUrls: ['./list-annonce-investisseur.page.scss'],
})
export class ListAnnonceInvestisseurPage implements OnInit {
  user:any;
  myAnnonces:any;
  annonces:any = [];

  constructor(private utilService: UtilsService,
    private storage: Storage,
    private constVariable: ConstvariableService,
    private annonceInvestisseurService: AnnonceInvestisseurService) {
      
     }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.storage.get('user').then((data)=>{
      console.log(data);
      this.user = data.id;
      this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
        loader.present();
        console.log(this.constVariable.endPoint+this.constVariable.makeAnnonceInvestisseur+"?user_id="+this.user);
        this.annonceInvestisseurService.getMyAnnonces(this.user).then((response)=>{
          this.myAnnonces = response.data;
          console.log(this.myAnnonces);
          this.myAnnonces.forEach(annonce => {
            let dateMonth = annonce.created_at;
            let dateArray = dateMonth.split(" ");
            let year = dateArray[0].split("-")[2];
            let month = dateArray[0].split("-")[1];
            let myDate = year+"/"+month;
  
            let objetAnnonce = {
              id:annonce.id,
              created_at:myDate,
              activite:annonce.secteuractivites,
              ville:annonce.ville.name,
              montant_a_investir:annonce.fourchette_montant_a_investir
            };
  
            this.annonces.push(objetAnnonce);
          });
          console.log(this.annonces);
          loader.dismiss();
        }).catch((error)=>{
          console.log(error);
          loader.dismiss();
          this.utilService.makeAlert("Business à vendre","Votre connexion semble avoir un problème").then((alert)=>{
            alert.present();
          })
        })
      })
    })
    
  }

  ionViewWillLeave() {
    this.annonces = [];
  }

  numStr(a, b) {
    a = '' + a;
    b = b || ' ';
    var c = '',
        d = 0;
    while (a.match(/^0[0-9]/)) {
      a = a.substr(1);
    }
    for (var i = a.length-1; i >= 0; i--) {
      c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
      d++;
    }
    return c;
  }
}
