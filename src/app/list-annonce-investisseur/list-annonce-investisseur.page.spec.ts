import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListAnnonceInvestisseurPage } from './list-annonce-investisseur.page';

describe('ListAnnonceInvestisseurPage', () => {
  let component: ListAnnonceInvestisseurPage;
  let fixture: ComponentFixture<ListAnnonceInvestisseurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAnnonceInvestisseurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListAnnonceInvestisseurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
