import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { RelationService } from "../services/relation.service";
import { UtilsService } from "../services/utils.service";
import { Storage } from '@ionic/storage';
import { AbonnementService } from "../services/abonnement.service";

@Component({
  selector: 'app-expert-more',
  templateUrl: './expert-more.page.html',
  styleUrls: ['./expert-more.page.scss'],
})
export class ExpertMorePage implements OnInit {
  datas:any = [];
  user_id:any;
  relation:any = {
    demandeur_id: "",
    receveur_id: "",
		attachable_type: "annonce cession",
		attachable_id: 1
  }
  constructor(private activeRoute: ActivatedRoute,
    private router: Router,
    private storage: Storage,
    private abonnementService: AbonnementService,
    private relationService: RelationService,
    private utilService: UtilsService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((data) => {
      if(data.id != "")
      {
        this.user_id = data.id;
      }
      else{
        this.utilService.makeToast('Veuillez vous connecter svp!').then((toast)=>{
          toast.present();
        });
      }
    });
    this.activeRoute.queryParams.subscribe(params => {
      let response = JSON.parse(params['data']);
      this.datas = response.data;
      console.log(this.datas);
    });
  }

  mise_en_relation(expert){
      console.log(expert);
      this.router.navigate(['/miseenreltaioncession', {
        'demandeur_id':this.user_id,
        'receveur_id':expert.id,
        'type':3
      }]);
  }
}
