import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExpertMorePageRoutingModule } from './expert-more-routing.module';

import { ExpertMorePage } from './expert-more.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpertMorePageRoutingModule
  ],
  declarations: [ExpertMorePage]
})
export class ExpertMorePageModule {}
