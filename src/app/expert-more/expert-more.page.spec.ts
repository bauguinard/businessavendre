import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExpertMorePage } from './expert-more.page';

describe('ExpertMorePage', () => {
  let component: ExpertMorePage;
  let fixture: ComponentFixture<ExpertMorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertMorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExpertMorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
