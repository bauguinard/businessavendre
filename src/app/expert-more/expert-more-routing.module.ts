import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpertMorePage } from './expert-more.page';

const routes: Routes = [
  {
    path: '',
    component: ExpertMorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpertMorePageRoutingModule {}
