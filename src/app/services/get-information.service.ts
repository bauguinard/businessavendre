import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class GetInformationService {

  constructor(private constVariable: ConstvariableService,
    private http: HTTP) { }

    categories(){
      return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.categorie, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }

    statut(){
      return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.statutAnnonce, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }

    typeTransaction(){
      return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.typeTransaction, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }

    zones(){
      return new Promise<any>((resolve,reject) => {
        this.http.get(this.constVariable.endPoint+this.constVariable.zone, {}, {}).then((data)=>{
            let resp = JSON.parse(data.data);
            resolve(resp);
          })
          .catch((error)=>{
            reject();
          })
              
        })
        .then((uu)=> uu)
        .catch(err => Promise.reject(err || 'err'));
    }

    
    pays(){
      return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.pays, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }

    ville(){
      return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.ville, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }

    getAllUserByType(typeUser,page){
      return new Promise<any>((resolve,reject) => {
        this.http.get(this.constVariable.endPoint+this.constVariable.getAllUserByType+"?type_id="+typeUser+"&page="+page, {}, {}).then((data)=>{
            let resp = JSON.parse(data.data);
            resolve(resp);
          })
          .catch((error)=>{
            reject();
          })
              
        })
        .then((uu)=> uu)
        .catch(err => {
          console.error(err);
          Promise.reject(err || 'err')
        });
    }


    typeSociete(){
      return new Promise<any>((resolve,reject) => {
        this.http.get(this.constVariable.endPoint+this.constVariable.typeSociete, {}, {}).then((data)=>{
            let resp = JSON.parse(data.data);
            resolve(resp);
          })
          .catch((error)=>{
            reject();
          })
              
        })
        .then((uu)=> uu)
        .catch(err => Promise.reject(err || 'err'));
    }
}
