import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstvariableService {

  public endPoint = "http://178.33.227.9:44399/Busness/";
  public register = "api/v1/users";
  public login = "api/auth/login";
  public update ="api/v1/users/"
  public zone = "api/v1/zones";
  public categorie = "api/v1/categoriesannonces";
  public statutAnnonce = "api/v1/statutsannonces";
  public typeTransaction = "api/v1/typetransaction";
  public typeSociete = "api/v1/typessocietes"
  public makeAnnonceCedant = "api/v1/annoncescessions";
  public makeAnnonceInvestisseur = "api/v1/annoncesinvestisseurs";
  public myAnnonces = "api/v1/annoncescessions?user_id=";
  public getAllUserByType ="api/v1/annonces";
  public formationDetail = "api/v1/blogs/";
  public pays = "api/v1/areas?division_id=1";
  public ville = "api/v1/areas?division_id=4";
  public formations = "api/v1/blogs?categorie_id=1";
  public publicite = "api/v1/blogs?categorie_id=2";
  public actualite = "api/v1/blogs?categorie_id=3";
  public blogDetail = "api/v1/blogs/"
  public offre = "api/v1/offres";
  public verificationSouscription = "api/v1/souscriptions/check-user/";
  public miseEnRelation = "api/v1/demandes";
  public verificationMiseEnrelation = "api/v1/check-relation?user_id=";
  public paiement = "api/v1/souscriptions";
  public tchat = "api/v1/chats";
  public detailSouscription ="api/v1/souscriptions?user_id=";
  public secteurActivite = "api/v1/secteursactivites";
  public urlPayment = "api/v1/souscriptions/get-url-digipay";
  constructor() { }
}
