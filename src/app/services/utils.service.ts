import { Injectable } from '@angular/core';
import { LoadingController, ActionSheetController, AlertController, PopoverController, Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private toastCtrl: ToastController,
		private popoverCtrl: PopoverController,
		private alertCtrl: AlertController,
		private actionSheetCtrl: ActionSheetController,
		private loadingCtrl: LoadingController) {

	}

	async makePopover(page: string, data?:any) {
		let popover = await this.popoverCtrl.create({
			component:page,
			event:data
		});
		return await popover;
	}

	async makeToast(msg: string, duration: number=3000, pos: any='bottom') {
	  let toast = await this.toastCtrl.create({
	    message: msg,
	    duration: duration,
	    position: pos
	  });

	  return toast;
	}

	async makeAlert(title: string, msg: string, subTitle: any = null, inputs: any[]=[],btns: any[]=['Ok'],dismiss=true) {
	  let alert = await this.alertCtrl.create({
	  	header: title,
    	subHeader: subTitle,
	    message: msg,
	    inputs: inputs,
	    buttons: btns,
	    backdropDismiss: dismiss
	  });

	  return await alert;
	}

	async makeActionSheet(title: string, btns: any[]=['Ok']) {

		btns.push({
			text: 'Annuler',
			role: 'cancel'
		});

		let actionSheet = await this.actionSheetCtrl.create({
			header: title,
			buttons: btns
		});

	  return await actionSheet;
	}

	async makeLoader(content: string) {
	  let loader = await this.loadingCtrl.create({
	  	message: content
	  });

	  return await loader;
	}
}
