import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";


@Injectable({
  providedIn: 'root'
})
export class AbonnementService {

  constructor(private http:HTTP,private constVariable: ConstvariableService) { }

  getAbonnement(){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.offre, {}, {}).then((response)=>{
          let resp = JSON.parse(response.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  getDetailOffre(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.offre+"/"+id, {}, {}).then((response)=>{
          let resp = JSON.parse(response.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  verificationSouscription(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.verificationSouscription+id, {}, {}).then((response)=>{
          let resp = JSON.parse(response.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  getMySouscription(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.detailSouscription+id, {}, {}).then((response)=>{
          let resp = JSON.parse(response.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  getUrlPayment(credentials){
    return new Promise<any>((resolve,reject) => {
      this.http.post(this.constVariable.endPoint+this.constVariable.urlPayment, credentials,{}).then((data)=>{
        let resp = JSON.parse(data.data);
        console.error(resp);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }
}
