import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class RelationService {

  constructor(private http:HTTP,private constVariable: ConstvariableService) { }

  miseEnRelation(credentials){
      return new Promise<any>((resolve,reject) => {
        this.http.post(this.constVariable.endPoint+this.constVariable.miseEnRelation, credentials,{}).then((response)=>{
          let resp = JSON.parse(response.data);
          resolve(resp);
        }).catch((error)=>{
          reject(error);
        })
      }).then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  getDemandeRelationReception(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+"api/v1/demandes?receveur_id="+id, {}, {}).then((response)=>{
        let resp = JSON.parse(response.data);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  getDemandeRelationDemandeur(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+"api/v1/demandes?demandeur_id="+id, {}, {}).then((response)=>{
        let resp = JSON.parse(response.data);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  getDetailRelation(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+"api/v1/demandes/"+id, {}, {}).then((response)=>{
        let resp = JSON.parse(response.data);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  validerDemande(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+"api/v1/demandes/accept-demande/"+id, {}, {}).then((response)=>{
        let resp = JSON.parse(response.data);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  annulerDemande(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+"api/v1/demandes/decline-demande/"+id, {}, {}).then((response)=>{
        let resp = JSON.parse(response.data);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  getRelationInfo(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+"api/v1/demandes/accept-demande/"+id, {}, {}).then((response)=>{
        let resp = JSON.parse(response.data);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  detailRelation(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+"api/v1/relations/"+id, {}, {}).then((response)=>{
        let resp = JSON.parse(response.data);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  

  
}
