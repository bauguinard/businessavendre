import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from './constvariable.service';

@Injectable({
  providedIn: 'root'
})
export class AnnonceInvestisseurService {

  constructor(private constVariable: ConstvariableService,
              private http: HTTP) { }

  sendAnnonce(credentials){
    return new Promise<any>((resolve,reject) => {
      this.http.post(this.constVariable.endPoint+this.constVariable.makeAnnonceInvestisseur, credentials,{}).then((data)=>{
        let resp = JSON.parse(data.data);
        console.error(resp);
        resolve(resp);
      }).catch((error)=>{
        reject(error);
      })
    }).then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  getMyAnnonces(user){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.makeAnnonceInvestisseur+"?user_id="+user, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  getMyDetailAnnonce(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.makeAnnonceInvestisseur+"/"+id, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  getLastAnnonceInvestisseur(){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.makeAnnonceInvestisseur+"?take=5", {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }

  removeAnnonce(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.makeAnnonceInvestisseur+"/"+id+"/delete", {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => {
        console.error(err);
        Promise.reject(err || 'err')
      });
  }

  getAllAnnonce(){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.makeAnnonceInvestisseur, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => {
        console.error(err);
        Promise.reject(err || 'err')
      });
  }

  search(data){
    return new Promise<any>((resolve,reject) => {
        this.http.get(this.constVariable.endPoint+this.constVariable.makeAnnonceInvestisseur+"?fourchette_montant_a_investir="+data.fourchette_montant_a_investir+"&ville_id="+data.ville_id+"&secteuractivite="+data.secteuractivite, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
      })
      .then((uu)=> uu)
      .catch(err => {
        console.error(err);
        Promise.reject(err || 'err')
      });
  }

  getSecteurActivite(){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.secteurActivite, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }
}
