import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private constVariable: ConstvariableService,
    private http: HTTP) { }

  register(credentials){
    return new Promise<any>((resolve,reject) => {
			this.http.post(this.constVariable.endPoint+this.constVariable.register, credentials,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  login(credentials){
    return new Promise<any>((resolve,reject) => {
			this.http.post(this.constVariable.endPoint+this.constVariable.login, credentials,{}).then((response)=>{
				let resp = JSON.parse(response.data);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  update(credentials){
	return new Promise<any>((resolve,reject) => {
		this.http.put(this.constVariable.endPoint+this.constVariable.update+credentials.id, credentials,{}).then((response)=>{
			let resp = JSON.parse(response.data);
			resolve(resp);
		}).catch((error)=>{
			reject(error);
		})
	}).then((uu)=> uu)
	.catch(err => Promise.reject(err || 'err'));
  }
}
