import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class PubliciteService {

  constructor(public constVariable: ConstvariableService, public http:HTTP) { }

  getPublicite(){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.publicite, {}, {}).then((response)=>{
          let resp = JSON.parse(response.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }
}
