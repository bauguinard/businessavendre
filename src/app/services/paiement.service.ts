import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class PaiementService {

  constructor(private constVariable: ConstvariableService,
    private http: HTTP) { }

    paiement(credentials){
      console.log(credentials);
      console.log(this.constVariable.endPoint+this.constVariable.paiement);
      return new Promise<any>((resolve,reject) => {
        this.http.post(this.constVariable.endPoint+this.constVariable.paiement, credentials,{}).then((response)=>{
          let resp = JSON.parse(response.data);
          resolve(resp);
        }).catch((error)=>{
          reject(error);
        })
      }).then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }
}
