import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class ExpertService {

  constructor(private constVariable: ConstvariableService,
    private http: HTTP) { }

    search(ville_id,domaine){
      return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+"api/v1/users?page=1&type_id=3&ville_id="+ville_id+"&domaine_competence="+domaine, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }
}
