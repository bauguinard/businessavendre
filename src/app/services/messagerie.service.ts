import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class MessagerieService {

  constructor(private constVariable: ConstvariableService,
    private http: HTTP) { }

    verification(user_id){
      console.log(this.constVariable.endPoint+this.constVariable.verificationMiseEnrelation+user_id);
      return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.verificationMiseEnrelation+user_id, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }


    send(credentials){
      return new Promise<any>((resolve,reject) => {
        this.http.post(this.constVariable.endPoint+this.constVariable.tchat, credentials,{}).then((data)=>{
          let resp = JSON.parse(data.data);
          console.error(resp);
          resolve(resp);
        }).catch((error)=>{
          reject(error);
        })
      }).then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
    }

    getMessage(relation_id){
        return new Promise<any>((resolve,reject) => {
          this.http.get(this.constVariable.endPoint+this.constVariable.tchat+"?relation_id="+relation_id, {}, {}).then((data)=>{
              let resp = JSON.parse(data.data);
              resolve(resp);
            })
            .catch((error)=>{
              reject();
            })
                
          })
          .then((uu)=> uu)
          .catch(err => Promise.reject(err || 'err'));
    }

    getRelation(user){
      console.error(this.constVariable.endPoint+this.constVariable.tchat+"?cedant_id="+user.id);
      return new Promise<any>((resolve,reject) => {
        this.http.get(this.constVariable.endPoint+this.constVariable.tchat+"?cedant_id="+user.id, {}, {}).then((data)=>{
            let resp = JSON.parse(data.data);
            resolve(resp);
          })
          .catch((error)=>{
            reject();
          })
              
        })
        .then((uu)=> uu)
        .catch(err => Promise.reject(err || 'err'));
    }

}
