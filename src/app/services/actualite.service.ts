import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class ActualiteService {

  constructor(private http:HTTP,private constVariable: ConstvariableService) { }

  getActualite(){
    return new Promise<any>((resolve,reject) => {
    this.http.get(this.constVariable.endPoint+this.constVariable.actualite, {}, {}).then((response)=>{
        let resp = JSON.parse(response.data);
        resolve(resp);
      })
      .catch((error)=>{
        reject();
      })
          
    })
    .then((uu)=> uu)
    .catch(err => Promise.reject(err || 'err'));
  }

  getDetailActualite(id){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.blogDetail+id, {}, {}).then((response)=>{
          let resp = JSON.parse(response.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => Promise.reject(err || 'err'));
  }
}
