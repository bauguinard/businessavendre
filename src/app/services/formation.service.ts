import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ConstvariableService } from "../services/constvariable.service";

@Injectable({
  providedIn: 'root'
})
export class FormationService {

  constructor(private constVariable: ConstvariableService,
    private http: HTTP) { }


  getFormations(){
    return new Promise<any>((resolve,reject) => {
      this.http.get(this.constVariable.endPoint+this.constVariable.formations, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => {
        console.error(err);
        Promise.reject(err || 'err')
      });
  }

  getFormationDetail(id){
    return new Promise<any>((resolve,reject) => {
      console.log(this.constVariable.endPoint+this.constVariable.formationDetail+id);
      this.http.get(this.constVariable.endPoint+this.constVariable.formationDetail+id, {}, {}).then((data)=>{
          let resp = JSON.parse(data.data);
          resolve(resp);
        })
        .catch((error)=>{
          reject();
        })
            
      })
      .then((uu)=> uu)
      .catch(err => {
        console.error(err);
        Promise.reject(err || 'err')
      });
  }
}
