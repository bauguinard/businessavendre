import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvestisseurDetailPageRoutingModule } from './investisseur-detail-routing.module';

import { InvestisseurDetailPage } from './investisseur-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvestisseurDetailPageRoutingModule
  ],
  declarations: [InvestisseurDetailPage]
})
export class InvestisseurDetailPageModule {}
