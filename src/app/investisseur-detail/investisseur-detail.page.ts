import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsService } from '../services/utils.service';
import { AnnonceInvestisseurService } from "../services/annonce-investisseur.service";
import { Storage } from '@ionic/storage';
import { RelationService } from '../services/relation.service';

@Component({
  selector: 'app-investisseur-detail',
  templateUrl: './investisseur-detail.page.html',
  styleUrls: ['./investisseur-detail.page.scss'],
})
export class InvestisseurDetailPage implements OnInit {
  id: number;
  private sub: any;
  user_id;
  myAnnonce:any ={
    id: "",
    fourchette_chriffes_affaires: "",
    fourchette_montant_a_investir: "",
    complementaire_transaction: "",
    active: "",
    pays_id: "",
    ville_id: "",
    user_id: "",
    statut_id:"",
    categorie_id:"",
    created_at: "",
    updated_at: "",
    typesociete:"",
    user:{
      id: "",
      name: "",
      email: "",
      permissions: "",
      is_activated: "",
      activated_at: "",
      last_login: "",
      created_at: "",
      updated_at: "",
      username: "",
      surname: "",
      deleted_at: "",
      last_seen: "",
      is_guest: "",
      is_superuser: "",
      civilite: "",
      date_naissance: "",
      telephone: "",
      adresse: "",
      nombre_relation_restante: "",
      date_fin_abonnement: "",
      type_id: "",
      pays_id: "",
      ville_id: ""
    },
    statut:{
      id: "",
      name: "",
      description: "",
      active: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    },
    categorie:{
      id: "",
      name: "",
      description: "",
      active: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    },
    pays:{
      id: "",
      name: "",
      zip: "",
      indicatif: "",
      description: "",
      active: "",
      area_id: "",
      division_id: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    },
    ville:{
      id: "",
      name: "",
      zip: "",
      indicatif: "",
      description: "",
      active: "",
      area_id: "",
      division_id: "",
      created_at: "",
      updated_at: "",
      deleted_at: ""
    }
  };
  relation:any = {
    demandeur_id: "",
    receveur_id: "",
		attachable_type: "annonce investisseur",
		attachable_id: 2
  }
  constructor(private route: ActivatedRoute,
    private utilService: UtilsService,
    private router: Router,
    private storage: Storage,
    private annonceInvestisseurService: AnnonceInvestisseurService,
    private relationService: RelationService) { }


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      console.log(this.id);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  ionViewWillEnter() {
    this.storage.get('user').then((data) => {
      console.log(data);
      console.log(typeof data);
      
      if(typeof data == "object")
      {
        this.user_id = data.id;
      }
    });

    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.annonceInvestisseurService.getMyDetailAnnonce(this.id).then((response)=>{
        console.log(response);
        this.myAnnonce = response.data;
        console.log(this.myAnnonce);
        loader.dismiss();
      })
      .catch(()=>{
        loader.dismiss()
      });
    })
  }

  remove(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      console.log(this.id);
      this.annonceInvestisseurService.removeAnnonce(this.id).then(()=>{
        loader.dismiss();
        this.router.navigate(['/investisseur-detail',this.id]);
      })
    })
  }

  mise_en_relation(){
    this.storage.set('pathPage',['/investisseur-detail',this.id]).then((success)=>{
      this.router.navigate(['/miseenreltaioncession', {
        'demandeur_id':this.user_id,
        'receveur_id':this.myAnnonce.user.id,
        'type':2
      }]);
    }); 
  }

  /*tchat(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.relation.demandeur_id = this.user_id;
      this.relation.receveur_id = this.myAnnonce.user.id;

      this.relationService.miseEnRelation(this.relation).then((response)=>{
        loader.dismiss();
        if (response.message != "created") {
          this.utilService.makeToast(response.message).then((toast)=>{
            toast.present();
            this.router.navigate(['/tchat-list-demandeur']);
          })
        }
        else{
          this.utilService.makeToast("Votre demande de mise en relation a bien été envoyée, elle est en attente de validation!").then((toast)=>{
            toast.present();
            this.router.navigate(['/tchat-list-demandeur']);
          })
        }
        
      }).catch((error)=>{
        loader.dismiss();
        this.utilService.makeToast("Nous n'avons pas pu vous en relation veuillez réessayer plus tard!").then((toast)=>{
          toast.present();
        })
      })
    })
    
  }*/

  numStr(a, b) {
    a = '' + a;
    b = b || ' ';
    var c = '',
        d = 0;
    while (a.match(/^0[0-9]/)) {
      a = a.substr(1);
    }
    for (var i = a.length-1; i >= 0; i--) {
      c = (d != 0 && d % 3 == 0) ? a[i] + b + c : a[i] + c;
      d++;
    }
    return c;
  }

}
