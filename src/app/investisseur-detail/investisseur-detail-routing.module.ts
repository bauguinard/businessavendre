import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvestisseurDetailPage } from './investisseur-detail.page';

const routes: Routes = [
  {
    path: '',
    component: InvestisseurDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvestisseurDetailPageRoutingModule {}
