import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpertDetailPage } from './expert-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ExpertDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExpertDetailPageRoutingModule {}
