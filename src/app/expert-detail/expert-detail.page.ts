import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-expert-detail',
  templateUrl: './expert-detail.page.html',
  styleUrls: ['./expert-detail.page.scss'],
})
export class ExpertDetailPage implements OnInit {

  contactExpert:boolean = false;
  constructor() { }

  ngOnInit() {
  }

  contact(){
    this.contactExpert = true;
  }
}
