import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExpertDetailPageRoutingModule } from './expert-detail-routing.module';

import { ExpertDetailPage } from './expert-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpertDetailPageRoutingModule
  ],
  declarations: [ExpertDetailPage]
})
export class ExpertDetailPageModule {}
