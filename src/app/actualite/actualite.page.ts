import { Component, OnInit } from '@angular/core';
import { ActualiteService } from "../services/actualite.service";
import { UtilsService } from '../services/utils.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-actualite',
  templateUrl: './actualite.page.html',
  styleUrls: ['./actualite.page.scss'],
})
export class ActualitePage implements OnInit {
  public actualites = [];

  constructor(private actualiteService: ActualiteService, private utilService: UtilsService,private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getActualite();
  }

  getActualite(){
    this.utilService.makeLoader('Patientez un instant svp!').then((loader)=>{
      loader.present();
      this.actualiteService.getActualite().then((response)=>{
        console.log(response.data);
        this.actualites = response.data;
        loader.dismiss();
      })
      .catch((err)=>{
        loader.dismiss();
      })
    })
  }

  detail(id){
    this.router.navigate(['/actualite-detail', id]);
  }

}
